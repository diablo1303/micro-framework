package bingosoft.metro.demo.api;

import java.util.Map;

import org.junit.Test;

import bingosoft.components.base.helper.LogHelper;
import bingosoft.components.base.service.ResultPage;
import bingosoft.metro.demo.form.DemoUserForm;
import bingosoft.metro.demo.service.DemoUserService;
import bingosoft.metro.demo.service.DemoUserServiceTest;
import leap.webunit.WebTestBase;
import leap.webunit.client.THttpResponse;

/**
 * @author feily
 * @功能描述 用户服务API测试类
 * @创建时间 2018-02-28 13:53:46
 */
public class DemoUesrControllerTest extends WebTestBase{

	/**
	 * 用户服务API URL地址
	 */
	protected static final String PATH = "/demoUser";
	
	/**
	 * 用户业务服务接口
	 */
	protected DemoUserService demoUserService = new DemoUserService();
	
	/**
	 * 测试分页查询用户 - success 无参数
	 */
	@Test
	public void testPageQueryDemoUsers() {
		THttpResponse response = get(PATH);
		response.assertContentNotEmpty().assertSuccess();

		ResultPage resultPage = response.decodeJson(ResultPage.class);
		LogHelper.println("resultPage", resultPage);
	}
	
	/**
	 * 测试分页查询用户 - success 普通过滤条件
	 */
	@Test
	public void testPageQueryDemoUsers_filter1() {
		THttpResponse response = get(PATH + "?rows=5&page=2&sidx=userName&sord=desc&t=1&filters=%7B%22groupOp%22%3A%22AND%22%2C%22rules%22%3A%5B%7B%22field%22%3A%22userName%22%2C%22op%22%3A%22cn%22%2C%22data%22%3A%221%22%7D%5D%7D");
		response.assertContentNotEmpty().assertSuccess();

		ResultPage resultPage = response.decodeJson(ResultPage.class);
		LogHelper.println("resultPage", resultPage);
	}
	
	/**
	 * 测试分页查询用户 - success date类型过滤条件
	 */
	@Test
	public void testPageQueryDemoUsers_filter2() {
		THttpResponse response = get(PATH + "?rows=5&page=2&sidx=userName&sord=desc&t=1&filters=%7b%22groupOp%22%3a%22AND%22%2c%22rules%22%3a%5b%7b%22field%22%3a%22createTime%22%2c%22op%22%3a%22gt%22%2c%22data%22%3a%222010-01-01%22%2c%22type%22%3a%22date%22%7d%5d%7d");
		response.assertContentNotEmpty().assertSuccess();

		ResultPage resultPage = response.decodeJson(ResultPage.class);
		LogHelper.println("resultPage", resultPage);
	}
	
	/**
	 * 测试分页查询用户 - success boolean类型过滤条件
	 */
	@Test
	public void testPageQueryDemoUsers_filter3() {
		THttpResponse response = get(PATH + "?rows=5&page=2&sidx=userName&sord=desc&t=1&filters=%7b%22groupOp%22%3a%22AND%22%2c%22rules%22%3a%5b%7b%22field%22%3a%22enable%22%2c%22op%22%3a%22eq%22%2c%22data%22%3a%22true%22%2c%22type%22%3a%22boolean%22%7d%5d%7d");
		response.assertContentNotEmpty().assertSuccess();

		ResultPage resultPage = response.decodeJson(ResultPage.class);
		LogHelper.println("resultPage", resultPage);
	}
	
	/**
	 * 测试分页查询用户 - success enableGenerateSql类型过滤条件
	 */
	@Test
	public void testPageQueryDemoUsers_filter4() {
		THttpResponse response = get(PATH + "?rows=5&page=2&sidx=userName&sord=desc&t=1&filters=%7b%22groupOp%22%3a%22AND%22%2c%22rules%22%3a%5b%7b%22field%22%3a%22enable%22%2c%22op%22%3a%22eq%22%2c%22data%22%3a%22true%22%2c%22type%22%3a%22boolean%22%2c+%22enableGenerateSql%22%3a%22false%22%7d%5d%7d");
		response.assertContentNotEmpty().assertSuccess();

		ResultPage resultPage = response.decodeJson(ResultPage.class);
		LogHelper.println("resultPage", resultPage);
	}
	
	/**
	 * 测试分页查询用户 - success in 类型过滤条件
	 */
	@Test
	public void testPageQueryDemoUsers_filter5() {
		THttpResponse response = get(PATH + "?rows=5&page=2&sidx=userName&sord=desc&t=1&filters=%7b%22groupOp%22%3a%22AND%22%2c%22rules%22%3a%5b%7b%22field%22%3a%22userName%22%2c%22op%22%3a%22in%22%2c%22data%22%3a%221%2c2%22%7d%5d%7d");
		response.assertContentNotEmpty().assertSuccess();

		ResultPage resultPage = response.decodeJson(ResultPage.class);
		LogHelper.println("resultPage", resultPage);
	}
	
	/**
	 * 测试创建用户 - success
	 */
	@Test
	public void testCreateDemoUser() {
		DemoUserForm demoUserForm = DemoUserServiceTest.buildDemoUserForm(1);
		THttpResponse response = postJson(PATH, demoUserForm);
		response.assertContentNotEmpty().assertSuccess();
		
		String message = response.getContent();
		LogHelper.println("message", message);
		assertEquals(message, this.buildMessage(DemoUserService.MESSAGE_CREATE_SUCCESS));
	}

	/**
	 * 测试修改用户 - fail ID不存在
	 */
	@Test
	public void testUpdateDemoUser_noExist() {
		String userId = "null";
		
		DemoUserForm demoUserForm = DemoUserServiceTest.buildDemoUserForm(1);
		demoUserForm.setUserId(userId);
		THttpResponse response = patchJson(PATH + "/" + userId, demoUserForm);
		response.assertContentNotEmpty().assertBadRequest();
		
		Map<String,Object> result = response.decodeJsonMap();
		String message = result.get("message").toString();
		LogHelper.println("message", message);
		assertEquals(message, DemoUserService.MESSAGE_GET_FAIL);
	}

	/**
	 * 测试修改用户 - success
	 */
	@Test
	public void testUpdateDemoUser() {
		// 获取存在的用户ID
		String userId = DemoUserServiceTest.getExistUserId();
		
		DemoUserForm demoUserForm = DemoUserServiceTest.buildDemoUserForm(1);
		demoUserForm.setUserId(userId);
		THttpResponse response = patchJson(PATH + "/" + userId, demoUserForm);
		response.assertContentNotEmpty().assertSuccess();
		
		String message = response.getContent();
		LogHelper.println("message", message);
		assertEquals(message, this.buildMessage(DemoUserService.MESSAGE_UPDATE_SUCCESS));
	}

	/**
	 * 测试获取用户 - fail ID不存在
	 */
	@Test
	public void testGetDemoUser_noExist() {
		String userId = "null";
		
		THttpResponse response = get(PATH + "/" + userId);
		response.assertContentNotEmpty().assertBadRequest();

		Map<String,Object> result = response.decodeJsonMap();
		String message = result.get("message").toString();
		LogHelper.println("message", message);
		assertEquals(message, DemoUserService.MESSAGE_GET_FAIL);
	}
	
	/**
	 * 测试获取用户 - success
	 */
	@Test
	public void testGetDemoUser() {
		// 获取存在的用户ID
		String userId = DemoUserServiceTest.getExistUserId();
		
		THttpResponse response = get(PATH + "/" + userId);
		response.assertContentNotEmpty().assertSuccess();

		DemoUserForm demoUserForm = response.decodeJson(DemoUserForm.class);
		LogHelper.println("demoUserForm", demoUserForm);
	}

	/**
	 * 测试删除用户 - fail ID不存在
	 */
	@Test
	public void testDeleteDemoUser_noExist() {
		String userId = "null";
		
		THttpResponse response = delete(PATH + "/" + userId);
		response.assertContentNotEmpty().assertBadRequest();
		
		Map<String,Object> result = response.decodeJsonMap();
		String message = result.get("message").toString();
		LogHelper.println("message", message);
		assertEquals(message, DemoUserService.MESSAGE_DELETE_FAIL);		
	}
	
	/**
	 * 测试删除用户 - success
	 */
	@Test
	public void testDeleteDemoUser() {
		// 获取存在的用户ID
		String userId = DemoUserServiceTest.getExistUserId();
		
		THttpResponse response = delete(PATH + "/" + userId);
		response.assertContentNotEmpty().assertSuccess();
		
		String message = response.getContent();
		LogHelper.println("message", message);
		assertEquals(message, this.buildMessage(DemoUserService.MESSAGE_DELETE_SUCCESS));		
	}
	
	protected Object buildMessage(String message) {
		return "\"" + message + "\"";
	}
}
