package bingosoft.metro.demo.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bingosoft.components.base.helper.LogHelper;
import bingosoft.components.base.service.PageQueryRequest;
import bingosoft.components.base.service.Result;
import bingosoft.components.base.service.ResultEntity;
import bingosoft.components.base.service.ResultPage;
import bingosoft.metro.demo.form.DemoUserForm;
import bingosoft.metro.demo.model.DemoUserModel;
import leap.core.junit.AppTestBase;
import leap.lang.Assert;

/**
 * @author feily
 * @功能描述 用户业务服务测试类
 * @创建时间 2018-02-28 13:53:46
 */
public class DemoUserServiceTest extends AppTestBase {

	/**
	 *  用户业务服务接口实现类
	 */
	private DemoUserService demoUserService = new DemoUserService();
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * 测试查询用户 - success
	 */
	@Test
	public void testQueryDemoUsers() {
		
		PageQueryRequest pageQueryRequest = new PageQueryRequest();
		Map<String, Object> params = new HashMap<String,Object>();
		ResultEntity<List<DemoUserForm>> result = this.demoUserService.queryDemoUsers(pageQueryRequest, params);
		
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
				
	}

	/**
	 * 测试分页查询用户 - success
	 */
	@Test
	public void testPageQueryDemoUsers() {
		PageQueryRequest pageQueryRequest = new PageQueryRequest();
		Map<String, Object> params = new HashMap<String,Object>();
		ResultEntity<ResultPage<List<DemoUserForm>>> result = this.demoUserService.pageQueryDemoUsers(pageQueryRequest, params);
		
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}

	/**
	 * 测试创建用户 - fail 参数为null
	 */
	@Test
	public void testCreateDemoUser_fail_null() {
		Result result = this.demoUserService.createDemoUser(null);
		Assert.isFalse(result.isSuccess());
		assertEquals(DemoUserService.MESSAGE_PARAM_OBJ_NULL, result.getMessage());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试创建用户 - success
	 */
	@Test
	public void testCreateDemoUser() {
		DemoUserForm demoUserForm = buildDemoUserForm(1);
		Result result = this.demoUserService.createDemoUser(demoUserForm);
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}

	/**
	 * 测试修改用户 - fail 参数为null
	 */
	@Test
	public void testUpdateDemoUser_fail_null() {
		Result result = this.demoUserService.updateDemoUser(null);
		Assert.isFalse(result.isSuccess());
		assertEquals(DemoUserService.MESSAGE_PARAM_OBJ_NULL, result.getMessage());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试修改用户 - fail ID不存在
	 */
	@Test
	public void testUpdateDemoUser_fail_noExist() {
		DemoUserForm demoUserForm = buildDemoUserForm(1);
		demoUserForm.setUserId("null");
		Result result = this.demoUserService.updateDemoUser(demoUserForm);
		Assert.isFalse(result.isSuccess());
		assertEquals(DemoUserService.MESSAGE_GET_FAIL, result.getMessage());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试修改用户 - success
	 */
	@Test
	public void testUpdateDemoUser() {
		DemoUserForm demoUserForm = buildDemoUserForm(1);
		Result result = this.demoUserService.createDemoUser(demoUserForm);
		Assert.isTrue(result.isSuccess());

		result = this.demoUserService.updateDemoUser(demoUserForm);
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}

	/**
	 * 测试获取用户 - fail 参数为null
	 */
	@Test
	public void testGetDemoUser_fail_null() {
		ResultEntity<DemoUserForm> resultEntity = this.demoUserService.getDemoUser(null);
		Assert.isFalse(resultEntity.isSuccess());
		assertEquals(DemoUserService.MESSAGE_PARAM_ID_NULL, resultEntity.getMessage());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试获取用户 - fail ID不存在
	 */
	@Test
	public void testGetDemoUser_fail_noExist() {
		ResultEntity<DemoUserForm> resultEntity = this.demoUserService.getDemoUser("null");
		Assert.isFalse(resultEntity.isSuccess());
		assertEquals(DemoUserService.MESSAGE_GET_FAIL, resultEntity.getMessage());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试获取用户 - success
	 */
	@Test
	public void testGetDemoUser() {
		String userId = getExistUserId();
		ResultEntity<DemoUserForm> resultEntity = this.demoUserService.getDemoUser(userId);
		Assert.isTrue(resultEntity.isSuccess());
		LogHelper.println("resultEntity", resultEntity);
	}

	/**
	 * 测试获取用户 - fail 参数为null
	 */
	@Test
	public void testGetDemoUserModel_fail_null() {
		ResultEntity<DemoUserModel> resultEntity = this.demoUserService.getDemoUserModel(null);
		Assert.isFalse(resultEntity.isSuccess());
		assertEquals(DemoUserService.MESSAGE_PARAM_ID_NULL, resultEntity.getMessage());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试获取用户 - fail ID不存在
	 */
	@Test
	public void testGetDemoUserModel_fail_noExist() {
		ResultEntity<DemoUserModel> resultEntity = this.demoUserService.getDemoUserModel("null");
		Assert.isFalse(resultEntity.isSuccess());
		assertEquals(DemoUserService.MESSAGE_GET_FAIL, resultEntity.getMessage());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试获取用户 - success
	 */
	@Test
	public void testGetDemoUserModel() {
		String userId = getExistUserId();
		ResultEntity<DemoUserModel> resultEntity = this.demoUserService.getDemoUserModel(userId);
		Assert.isTrue(resultEntity.isSuccess());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试删除用户 - fail 参数为null
	 */
	@Test
	public void testDeleteDemoUser_fail_null() {
		Result result = this.demoUserService.deleteDemoUser(null);
		Assert.isFalse(result.isSuccess());
		assertEquals(DemoUserService.MESSAGE_PARAM_ID_NULL, result.getMessage());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试删除用户 - fail ID不存在
	 */
	@Test
	public void testDeleteDemoUser_fail_noExist() {
		Result result = this.demoUserService.deleteDemoUser("null");
		Assert.isFalse(result.isSuccess());
		assertEquals(DemoUserService.MESSAGE_DELETE_FAIL, result.getMessage());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试删除用户 - success
	 */
	@Test
	public void testDeleteDemoUser() {
		String userId = getExistUserId();
		Result result = this.demoUserService.deleteDemoUser(userId);
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}

	/**
	 * 构建用户
	 * @param i
	 * @return
	 */
	public static DemoUserForm buildDemoUserForm(int i) {
		DemoUserForm demoUserForm = new DemoUserForm();
		//demoUserForm.setUserId(ObjectHelper.getUUID());
		demoUserForm.setUserName("userName" + i);
		demoUserForm.setBirthday(new Date());
		demoUserForm.setAge(i);
		demoUserForm.setWeight(1.0 * i);
		demoUserForm.setEnable(true);
		demoUserForm.setRemark("remark" + i);
		
		return demoUserForm;
	}
	
	/**
	 * 获取存在的用户ID
	 * @return
	 */
	public static String getExistUserId() {
		DemoUserForm demoUserForm = buildDemoUserForm(1);
		Result result = new DemoUserService().createDemoUser(demoUserForm);
		Assert.isTrue(result.isSuccess());

		return demoUserForm.getUserId();
	}
}
