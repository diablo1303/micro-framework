package bingosoft.metro.demo.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bingosoft.components.base.form.ActionForm;
import bingosoft.components.base.helper.LogHelper;
import bingosoft.components.base.service.PageQueryRequest;
import bingosoft.components.base.service.Result;
import bingosoft.components.base.service.ResultEntity;
import bingosoft.components.base.service.ResultPage;
import bingosoft.metro.demo.form.DemoDepartmentForm;
import bingosoft.metro.demo.form.DemoUserForm;
import bingosoft.metro.demo.model.DemoDepartmentModel;
import leap.core.junit.AppTestBase;
import leap.lang.Assert;

/**
 * @author feily
 * @功能描述 组织业务服务测试类
 * @创建时间 2018-03-02 02:23:52
 */
public class DemoDepartmentServiceTest extends AppTestBase {

	/**
	 *  组织业务服务接口实现类
	 */
	private DemoDepartmentService demoDepartmentService = new DemoDepartmentService();
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * 测试查询组织 - success
	 */
	@Test
	public void testQueryDemoDepartments() {
		
		PageQueryRequest pageQueryRequest = new PageQueryRequest();
		Map<String, Object> params = new HashMap<String,Object>();
		ResultEntity<List<DemoDepartmentForm>> result = this.demoDepartmentService.queryDemoDepartments(pageQueryRequest, params);
		
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}

	/**
	 * 测试分页查询组织 - success
	 */
	@Test
	public void testPageQueryDemoDepartments() {
		PageQueryRequest pageQueryRequest = new PageQueryRequest();
		Map<String, Object> params = new HashMap<String,Object>();
		ResultEntity<ResultPage<List<DemoDepartmentForm>>> result = this.demoDepartmentService.pageQueryDemoDepartments(pageQueryRequest, params);
		
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}

	/**
	 * 测试创建组织 - fail 参数为null
	 */
	@Test
	public void testCreateDemoDepartment_fail_null() {
		Result result = this.demoDepartmentService.createDemoDepartment(null);
		Assert.isFalse(result.isSuccess());
		assertEquals(DemoDepartmentService.MESSAGE_PARAM_OBJ_NULL, result.getMessage());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试创建组织 - success
	 */
	@Test
	public void testCreateDemoDepartment() {
		DemoDepartmentForm demoDepartmentForm = buildDemoDepartmentForm(1);
		Result result = this.demoDepartmentService.createDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试创建组织和用户 - success
	 */
	@Test
	public void testCreateDemoDepartment_users() {
		DemoDepartmentForm demoDepartmentForm = buildDemoDepartmentForm(1);
		demoDepartmentForm.getDemoUserForms().add(buildDemoUserForm(1, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(buildDemoUserForm(2, ActionForm.ACTION_TYPE_INSERT));
		
		Result result = this.demoDepartmentService.createDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}

	/**
	 * 测试修改组织 - fail 参数为null
	 */
	@Test
	public void testUpdateDemoDepartment_fail_null() {
		Result result = this.demoDepartmentService.updateDemoDepartment(null);
		Assert.isFalse(result.isSuccess());
		assertEquals(DemoDepartmentService.MESSAGE_PARAM_OBJ_NULL, result.getMessage());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试修改组织 - fail ID不存在
	 */
	@Test
	public void testUpdateDemoDepartment_fail_noExist() {
		DemoDepartmentForm demoDepartmentForm = buildDemoDepartmentForm(1);
		demoDepartmentForm.setDepartmentId("null");
		Result result = this.demoDepartmentService.updateDemoDepartment(demoDepartmentForm);
		Assert.isFalse(result.isSuccess());
		assertEquals(DemoDepartmentService.MESSAGE_GET_FAIL, result.getMessage());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试修改组织 - success
	 */
	@Test
	public void testUpdateDemoDepartment() {
		DemoDepartmentForm demoDepartmentForm = buildDemoDepartmentForm(1);
		Result result = this.demoDepartmentService.createDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());

		result = this.demoDepartmentService.updateDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试修改组织和用户 - success
	 */
	@Test
	public void testUpdateDemoDepartment_users() {
		DemoDepartmentForm demoDepartmentForm = buildDemoDepartmentForm(1);
		demoDepartmentForm.getDemoUserForms().add(buildDemoUserForm(1, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(buildDemoUserForm(2, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(buildDemoUserForm(3, ActionForm.ACTION_TYPE_INSERT));
		Result result = this.demoDepartmentService.createDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());

		// 处理用户数据
		demoDepartmentForm.getDemoUserForms().get(0).setActionType(ActionForm.ACTION_TYPE_DELETE); // 删除
		demoDepartmentForm.getDemoUserForms().get(1).setActionType(ActionForm.ACTION_TYPE_UPDATE); // 修改
		demoDepartmentForm.getDemoUserForms().remove(2); // 不变
		demoDepartmentForm.getDemoUserForms().add(buildDemoUserForm(4, ActionForm.ACTION_TYPE_INSERT)); // 新增
		
		result = this.demoDepartmentService.updateDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}

	/**
	 * 测试获取组织 - fail 参数为null
	 */
	@Test
	public void testGetDemoDepartment_fail_null() {
		ResultEntity<DemoDepartmentForm> resultEntity = this.demoDepartmentService.getDemoDepartment(null);
		Assert.isFalse(resultEntity.isSuccess());
		assertEquals(DemoDepartmentService.MESSAGE_PARAM_ID_NULL, resultEntity.getMessage());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试获取组织 - fail ID不存在
	 */
	@Test
	public void testGetDemoDepartment_fail_noExist() {
		ResultEntity<DemoDepartmentForm> resultEntity = this.demoDepartmentService.getDemoDepartment("null");
		Assert.isFalse(resultEntity.isSuccess());
		assertEquals(DemoDepartmentService.MESSAGE_GET_FAIL, resultEntity.getMessage());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试获取组织 - success
	 */
	@Test
	public void testGetDemoDepartment() {
		String departmentId = getExistDepartmentId();
		ResultEntity<DemoDepartmentForm> resultEntity = this.demoDepartmentService.getDemoDepartment(departmentId);
		Assert.isTrue(resultEntity.isSuccess());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试获取组织和用户 - success
	 */
	@Test
	public void testGetDemoDepartment_users() {
		DemoDepartmentForm demoDepartmentForm = buildDemoDepartmentForm(1);
		demoDepartmentForm.getDemoUserForms().add(buildDemoUserForm(1, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(buildDemoUserForm(2, ActionForm.ACTION_TYPE_INSERT));
		
		Result result = this.demoDepartmentService.createDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());
		
		String departmentId = demoDepartmentForm.getDepartmentId();
		ResultEntity<DemoDepartmentForm> resultEntity = this.demoDepartmentService.getDemoDepartment(departmentId);
		Assert.isTrue(resultEntity.isSuccess());
		LogHelper.println("resultEntity", resultEntity);
	}

	/**
	 * 测试获取组织 - fail 参数为null
	 */
	@Test
	public void testGetDemoDepartmentModel_fail_null() {
		ResultEntity<DemoDepartmentModel> resultEntity = this.demoDepartmentService.getDemoDepartmentModel(null);
		Assert.isFalse(resultEntity.isSuccess());
		assertEquals(DemoDepartmentService.MESSAGE_PARAM_ID_NULL, resultEntity.getMessage());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试获取组织 - fail ID不存在
	 */
	@Test
	public void testGetDemoDepartmentModel_fail_noExist() {
		ResultEntity<DemoDepartmentModel> resultEntity = this.demoDepartmentService.getDemoDepartmentModel("null");
		Assert.isFalse(resultEntity.isSuccess());
		assertEquals(DemoDepartmentService.MESSAGE_GET_FAIL, resultEntity.getMessage());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试获取组织 - success
	 */
	@Test
	public void testGetDemoDepartmentModel() {
		String departmentId = getExistDepartmentId();
		ResultEntity<DemoDepartmentModel> resultEntity = this.demoDepartmentService.getDemoDepartmentModel(departmentId);
		Assert.isTrue(resultEntity.isSuccess());
		LogHelper.println("resultEntity", resultEntity);
	}
	
	/**
	 * 测试删除组织 - fail 参数为null
	 */
	@Test
	public void testDeleteDemoDepartment_fail_null() {
		Result result = this.demoDepartmentService.deleteDemoDepartment(null);
		Assert.isFalse(result.isSuccess());
		assertEquals(DemoDepartmentService.MESSAGE_PARAM_ID_NULL, result.getMessage());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试删除组织 - fail ID不存在
	 */
	@Test
	public void testDeleteDemoDepartment_fail_noExist() {
		Result result = this.demoDepartmentService.deleteDemoDepartment("null");
		Assert.isFalse(result.isSuccess());
		assertEquals(DemoDepartmentService.MESSAGE_DELETE_FAIL, result.getMessage());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试删除组织 - success
	 */
	@Test
	public void testDeleteDemoDepartment() {
		String departmentId = getExistDepartmentId();
		Result result = this.demoDepartmentService.deleteDemoDepartment(departmentId);
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}
	
	/**
	 * 测试删除组织和用户 - success
	 */
	@Test
	public void testDeleteDemoDepartment_users() {
		DemoDepartmentForm demoDepartmentForm = buildDemoDepartmentForm(1);
		demoDepartmentForm.getDemoUserForms().add(buildDemoUserForm(1, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(buildDemoUserForm(2, ActionForm.ACTION_TYPE_INSERT));
		
		Result result = this.demoDepartmentService.createDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());
		
		String departmentId = demoDepartmentForm.getDepartmentId();
		result = this.demoDepartmentService.deleteDemoDepartment(departmentId);
		Assert.isTrue(result.isSuccess());
		LogHelper.println("result", result);
	}

	/**
	 * 构建组织
	 * @param i
	 * @return
	 */
	public static DemoDepartmentForm buildDemoDepartmentForm(int i) {
		DemoDepartmentForm demoDepartmentForm = new DemoDepartmentForm();
//		demoDepartmentForm.setDepartmentId("DepartmentId" + i);
//		demoDepartmentForm.setParentDeptId("ParentDeptId" + i);
		demoDepartmentForm.setDepartmentName("DepartmentName" + i);
		demoDepartmentForm.setWbsCode("WbsCode" + i);
		demoDepartmentForm.setEnable(true);
		demoDepartmentForm.setRemark("Remark" + i);

		return demoDepartmentForm;
	}
	
	/**
	 * 构建用户
	 * @param i
	 * @param actionType
	 * @return
	 */
	public static DemoUserForm buildDemoUserForm(int i, String actionType) {
		DemoUserForm demoUserForm = new DemoUserForm();
		//demoUserForm.setUserId(ObjectHelper.getUUID());
		demoUserForm.setUserName("userName" + i);
		demoUserForm.setBirthday(new Date());
		demoUserForm.setAge(i);
		demoUserForm.setWeight(1.0 * i);
		demoUserForm.setEnable(true);
		demoUserForm.setRemark("remark" + i);
		demoUserForm.setActionType(actionType);
		
		return demoUserForm;
	}
	
	/**
	 * 获取存在的组织ID
	 * @return
	 */
	public static String getExistDepartmentId() {
		DemoDepartmentForm demoDepartmentForm = buildDemoDepartmentForm(1);
		Result result = new DemoDepartmentService().createDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());

		return demoDepartmentForm.getDepartmentId();
	}
}