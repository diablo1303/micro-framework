package bingosoft.metro.demo.api;

import java.util.Map;

import org.junit.Test;

import bingosoft.components.base.form.ActionForm;
import bingosoft.components.base.helper.LogHelper;
import bingosoft.components.base.service.Result;
import bingosoft.components.base.service.ResultPage;
import bingosoft.metro.demo.form.DemoDepartmentForm;
import bingosoft.metro.demo.service.DemoDepartmentService;
import bingosoft.metro.demo.service.DemoDepartmentServiceTest;
import leap.lang.Assert;
import leap.webunit.WebTestBase;
import leap.webunit.client.THttpResponse;

/**
 * @author feily
 * @功能描述 组织服务API测试类
 * @创建时间 2018-03-02 02:23:52
 */
public class DemoDepartmentControllerTest extends WebTestBase{

	/**
	 * 组织服务API URL地址
	 */
	protected static final String PATH = "/demoDepartment";
	
	/**
	 * 组织业务服务接口
	 */
	protected DemoDepartmentService demoDepartmentService = new DemoDepartmentService();
	
	/**
	 * 测试分页查询组织 - success
	 */
	@Test
	public void testPageQueryDemoDepartments() {
		THttpResponse response = get(PATH + "?page_size=5&page=2&t=1");
		response.assertContentNotEmpty().assertSuccess();

		ResultPage resultPage = response.decodeJson(ResultPage.class);
		LogHelper.println("resultPage", resultPage);
	}
	
	/**
	 * 测试创建组织 - success
	 */
	@Test
	public void testCreateDemoDepartment() {
		DemoDepartmentForm demoDepartmentForm = DemoDepartmentServiceTest.buildDemoDepartmentForm(1);
		THttpResponse response = postJson(PATH, demoDepartmentForm);
		response.assertContentNotEmpty().assertSuccess();
		
		String message = response.getContent();
		LogHelper.println("message", message);
		assertEquals(message, this.buildMessage(DemoDepartmentService.MESSAGE_CREATE_SUCCESS));
	}
	
	/**
	 * 测试创建组织和用户 - success
	 */
	@Test
	public void testCreateDemoDepartment_users() {
		DemoDepartmentForm demoDepartmentForm = DemoDepartmentServiceTest.buildDemoDepartmentForm(1);
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(1, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(2, ActionForm.ACTION_TYPE_INSERT));
		
		THttpResponse response = postJson(PATH, demoDepartmentForm);
		response.assertContentNotEmpty().assertSuccess();
		
		String message = response.getContent();
		LogHelper.println("message", message);
		assertEquals(message, this.buildMessage(DemoDepartmentService.MESSAGE_CREATE_SUCCESS));
	}

	/**
	 * 测试修改组织 - fail ID不存在
	 */
	@Test
	public void testUpdateDemoDepartment_noExist() {
		String departmentId = "null";
		
		DemoDepartmentForm demoDepartmentForm = DemoDepartmentServiceTest.buildDemoDepartmentForm(1);
		demoDepartmentForm.setDepartmentId(departmentId);
		THttpResponse response = patchJson(PATH + "/" + departmentId, demoDepartmentForm);
		response.assertContentNotEmpty().assertBadRequest();
		
		Map<String,Object> result = response.decodeJsonMap();
		String message = result.get("message").toString();
		LogHelper.println("message", message);
		assertEquals(message, DemoDepartmentService.MESSAGE_GET_FAIL);
	}

	/**
	 * 测试修改组织 - success
	 */
	@Test
	public void testUpdateDemoDepartment() {
		// 获取存在的组织ID
		String departmentId = DemoDepartmentServiceTest.getExistDepartmentId();
		
		DemoDepartmentForm demoDepartmentForm = DemoDepartmentServiceTest.buildDemoDepartmentForm(1);
		demoDepartmentForm.setDepartmentId(departmentId);
		THttpResponse response = patchJson(PATH + "/" + departmentId, demoDepartmentForm);
		response.assertContentNotEmpty().assertSuccess();
		
		String message = response.getContent();
		LogHelper.println("message", message);
		assertEquals(message, this.buildMessage(DemoDepartmentService.MESSAGE_UPDATE_SUCCESS));
	}
	
	/**
	 * 测试修改组织和用户 - success
	 */
	@Test
	public void testUpdateDemoDepartment_users() {
		
		DemoDepartmentForm demoDepartmentForm = DemoDepartmentServiceTest.buildDemoDepartmentForm(1);
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(1, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(2, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(3, ActionForm.ACTION_TYPE_INSERT));
		Result result = this.demoDepartmentService.createDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());

		// 处理用户数据
		demoDepartmentForm.getDemoUserForms().get(0).setActionType(ActionForm.ACTION_TYPE_DELETE); // 删除
		demoDepartmentForm.getDemoUserForms().get(1).setActionType(ActionForm.ACTION_TYPE_UPDATE); // 修改
		demoDepartmentForm.getDemoUserForms().remove(2); // 不变
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(4, ActionForm.ACTION_TYPE_INSERT)); // 新增
		
		// 获取存在的组织ID
		String departmentId = demoDepartmentForm.getDepartmentId();
		THttpResponse response = patchJson(PATH + "/" + departmentId, demoDepartmentForm);
		response.assertContentNotEmpty().assertSuccess();
		
		String message = response.getContent();
		LogHelper.println("message", message);
		assertEquals(message, this.buildMessage(DemoDepartmentService.MESSAGE_UPDATE_SUCCESS));
	}

	/**
	 * 测试获取组织 - fail ID不存在
	 */
	@Test
	public void testGetDemoDepartment_noExist() {
		String departmentId = "null";
		
		THttpResponse response = get(PATH + "/" + departmentId);
		response.assertContentNotEmpty().assertBadRequest();

		Map<String,Object> result = response.decodeJsonMap();
		String message = result.get("message").toString();
		LogHelper.println("message", message);
		assertEquals(message, DemoDepartmentService.MESSAGE_GET_FAIL);
	}
	
	/**
	 * 测试获取组织 - success
	 */
	@Test
	public void testGetDemoDepartment() {
		// 获取存在的组织ID
		String departmentId = DemoDepartmentServiceTest.getExistDepartmentId();
		
		THttpResponse response = get(PATH + "/" + departmentId);
		response.assertContentNotEmpty().assertSuccess();

		DemoDepartmentForm demoDepartmentForm = response.decodeJson(DemoDepartmentForm.class);
		LogHelper.println("demoDepartmentForm", demoDepartmentForm);
	}
	
	/**
	 * 测试获取组织和用户 - success
	 */
	@Test
	public void testGetDemoDepartment_users() {
		// 获取存在的组织ID
		DemoDepartmentForm demoDepartmentForm = DemoDepartmentServiceTest.buildDemoDepartmentForm(1);
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(1, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(2, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(3, ActionForm.ACTION_TYPE_INSERT));
		Result result = this.demoDepartmentService.createDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());
		String departmentId = demoDepartmentForm.getDepartmentId();
		
		THttpResponse response = get(PATH + "/" + departmentId);
		response.assertContentNotEmpty().assertSuccess();

		demoDepartmentForm = response.decodeJson(DemoDepartmentForm.class);
		LogHelper.println("demoDepartmentForm", demoDepartmentForm);
	}

	/**
	 * 测试删除组织 - fail ID不存在
	 */
	@Test
	public void testDeleteDemoDepartment_noExist() {
		String departmentId = "null";
		
		THttpResponse response = delete(PATH + "/" + departmentId);
		response.assertContentNotEmpty().assertBadRequest();
		
		Map<String,Object> result = response.decodeJsonMap();
		String message = result.get("message").toString();
		LogHelper.println("message", message);
		assertEquals(message, DemoDepartmentService.MESSAGE_DELETE_FAIL);		
	}
	
	/**
	 * 测试删除组织 - success
	 */
	@Test
	public void testDeleteDemoDepartment() {
		// 获取存在的组织ID
		String departmentId = DemoDepartmentServiceTest.getExistDepartmentId();
		
		THttpResponse response = delete(PATH + "/" + departmentId);
		response.assertContentNotEmpty().assertSuccess();
		
		String message = response.getContent();
		LogHelper.println("message", message);
		assertEquals(message, this.buildMessage(DemoDepartmentService.MESSAGE_DELETE_SUCCESS));		
	}
	
	/**
	 * 测试删除组织和用户 - success
	 */
	@Test
	public void testDeleteDemoDepartment_users() {
		// 获取存在的组织ID
		DemoDepartmentForm demoDepartmentForm = DemoDepartmentServiceTest.buildDemoDepartmentForm(1);
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(1, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(2, ActionForm.ACTION_TYPE_INSERT));
		demoDepartmentForm.getDemoUserForms().add(DemoDepartmentServiceTest.buildDemoUserForm(3, ActionForm.ACTION_TYPE_INSERT));
		Result result = this.demoDepartmentService.createDemoDepartment(demoDepartmentForm);
		Assert.isTrue(result.isSuccess());
		String departmentId = demoDepartmentForm.getDepartmentId();
		
		THttpResponse response = delete(PATH + "/" + departmentId);
		response.assertContentNotEmpty().assertSuccess();
		
		String message = response.getContent();
		LogHelper.println("message", message);
		assertEquals(message, this.buildMessage(DemoDepartmentService.MESSAGE_DELETE_SUCCESS));		
	}
	
	protected Object buildMessage(String message) {
		return "\"" + message + "\"";
	}
}