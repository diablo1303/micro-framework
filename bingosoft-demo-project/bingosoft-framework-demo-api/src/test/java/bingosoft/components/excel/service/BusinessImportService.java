package bingosoft.components.excel.service;

import java.util.Map;

import bingosoft.components.base.helper.ConvertHelper;
import bingosoft.components.base.service.ServiceResult;
import bingosoft.components.excel.entity.ExcelCell;

public class BusinessImportService extends ExcelImportService {

	public BusinessImportService() {
		this.setLogClass(this.getClass());

		this.logMethodCalled("BusinessImportService");
	}

	@Override
	protected ServiceResult validateBusinessData(ExcelCell excelCell, Map<String, Object> dataRow, Object value) {
		this.logMethodCalled("ValidateBusinessData");

		ServiceResult result = new ServiceResult();

		// 工作页ExcelSheetCode1的区间SheetAreaCode1的【用户姓名】不能为“张三”
		if (excelCell.getParent().getParent().getExcelSheetCode().equals("ExcelSheetCode1")
				&& excelCell.getParent().getSheetAreaCode().equals("SheetAreaCode1")
				&& excelCell.getFieldCode().equals("Name")) {
			String strValue = ConvertHelper.convertToString(value);
			if (strValue.equals("张三")) {
				result.setIsSuccess(false);
				result.setMessage("用户姓名不能为“张三”！");
				return result;
			}
			
			if (strValue.contains(".")) {
				result.setIsSuccess(false);
				result.setMessage("用户姓名不能含有“.”！");
				return result;
			}
		}
	
		return result;
	}

}
