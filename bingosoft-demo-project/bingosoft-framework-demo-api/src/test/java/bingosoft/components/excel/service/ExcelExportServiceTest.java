package bingosoft.components.excel.service;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bingosoft.components.base.service.ServiceResult;
import bingosoft.components.excel.entity.ExcelCell;
import bingosoft.components.excel.form.ExcelField;
import bingosoft.components.excel.model.TestUserModel;
import leap.orm.model.Model;

public class ExcelExportServiceTest {

	private static String currentPath;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		currentPath = "D:\\test\\";
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testExportExcelStringListOfExcelFieldListOfModel() throws Exception {
		ExcelExportService target = new ExcelExportService();

		//String exportFilePath = currentPath + "Excel_Export.xls";
		//String exportFilePath = currentPath + "Excel_Export.xlsx";
		String exportFilePath = currentPath + "Excel_Export.et";
		List<ExcelField> excelFields = this.buildExcelFields();
		List<Model> excelValues = this.buildExcelValues();
		
		ServiceResult actual = target.exportExcel(exportFilePath, excelFields, excelValues);
		
		assertTrue(actual.getIsSuccess());
		
		File file = new File(exportFilePath);
		assertTrue(file.exists());
	}

	private List<Model> buildExcelValues() {
		List<Model> list = new ArrayList<Model>();
		
		for (int i = 1; i <= 20; i++) {
			TestUserModel user = new TestUserModel();
			user.setUserId("userId" + i);
			user.setUserName("userName" + i);
			user.setAge(10 + i);
			user.setBirthday(new Date());
			user.setWeight(10.0 + i * 1.1);
			user.setEnable(true);
			user.setRemark("remark" + i);
			user.setCreateTime(new Timestamp(new Date().getTime()));
			list.add(user);
		}
		
		return list;
	}

	private List<ExcelField> buildExcelFields() {
		
		ExcelField f1 = new ExcelField("姓名", "userName");
		ExcelField f2 = new ExcelField("年龄", "age", ExcelCell.DataType_Numeric);
		ExcelField f3 = new ExcelField("生日", "birthday", ExcelCell.DataType_DateTime);
		ExcelField f4 = new ExcelField("体重", "weight", ExcelCell.DataType_Numeric, "0.0");
		ExcelField f5 = new ExcelField("是否", "enable", ExcelCell.DataType_Boolean);
		ExcelField f6 = new ExcelField("创建时间", "createTime", ExcelCell.DataType_DateTime, "yyyy-mm-dd hh:mm:ss");
		
		List<ExcelField> list = new ArrayList<ExcelField>();
		list.add(f1);
		list.add(f2);
		list.add(f3);
		list.add(f4);
		list.add(f5);
		list.add(f6);
		
		return list;
	}

}
