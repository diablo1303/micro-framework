package bingosoft.components.excel.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bingosoft.components.base.helper.LogHelper;
import bingosoft.components.base.service.ServiceResult;
import bingosoft.components.excel.ExcelFactory;
import bingosoft.components.excel.entity.ExcelCell;
import bingosoft.components.excel.entity.ExcelInfo;
import bingosoft.components.excel.entity.ExcelSheet;
import bingosoft.components.excel.entity.SheetArea;

public class ExcelImportServiceTest {

	private static String currentPath;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// ClassLoader classLoader =
		// ExcelImportServiceTest.class.getClassLoader();
		// URL url = classLoader.getResource("");
		// currentPath = url.getFile();
		currentPath = "D:\\test\\";
	}

	@Before
	public void setUp() throws Exception {
	}

	/**
	 * 无验证
	 */
	@Test
	public void testImportExcel_NoValidate() {
		ExcelImportService target = new ExcelImportService();

		ExcelInfo excelInfo = ExcelFactory.getExcelInfo("DemoExcelImport");
		for (ExcelSheet sheet : excelInfo.getSheets()) {
			for (SheetArea area : sheet.getAreas()) {
				for (ExcelCell cell : area.getCells()) {
					cell.setValidator("");
				}
			}
		}

		// String importFilePath = currentPath + "Excel_NoValidate.xls";
		//String importFilePath = currentPath + "Excel_NoValidate.xlsx";
		String importFilePath = currentPath + "Excel_NoValidate.et";
		ServiceResult actual = target.importExcel(importFilePath, excelInfo);

		Map<String, Object> data = (Map<String, Object>) actual.getBusinessObject();

		LogHelper.println("data", data);

		assertTrue(actual.getIsSuccess());
	}

	/**
	 * 基本格式验证,验证不通过
	 */
	@Test
	public void testImportExcel_FormatValidateFail() {
		ExcelImportService target = new ExcelImportService();

		ExcelInfo excelInfo = ExcelFactory.getExcelInfo("DemoExcelImport");

		// String importFilePath = currentPath + "Excel_FormatValidate.xls";
		String importFilePath = currentPath + "Excel_FormatValidate.xlsx";
		ServiceResult actual = target.importExcel(importFilePath, excelInfo);

		LogHelper.println("actual", actual);

		assertFalse(actual.getIsSuccess());
	}

	/**
	 * 基本格式验证,验证通过
	 */
	@Test
	public void testImportExcel_FormatValidateSuccess() {
		ExcelImportService target = new ExcelImportService();

		ExcelInfo excelInfo = ExcelFactory.getExcelInfo("DemoExcelImport");

		// String importFilePath = currentPath + "Excel_FormatValidatePass.xls";
		String importFilePath = currentPath + "Excel_FormatValidatePass.xlsx";
		ServiceResult actual = target.importExcel(importFilePath, excelInfo);

		LogHelper.println("actual", actual);

		assertTrue(actual.getIsSuccess());
	}

	/**
	 * 自定义业务验证,验证不通过
	 */
	@Test
	public void testImportExcel_BusinessValidateFail() {
		BusinessImportService target = new BusinessImportService();

		ExcelInfo excelInfo = ExcelFactory.getExcelInfo("DemoExcelImport");

		// String importFilePath = currentPath + "Excel_FormatValidatePass.xls";
		String importFilePath = currentPath + "Excel_FormatValidatePass.xlsx";
		ServiceResult actual = target.importExcel(importFilePath, excelInfo);

		LogHelper.println("actual", actual);

		assertFalse(actual.getIsSuccess());
	}
}
