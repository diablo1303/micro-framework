package bingosoft.components.excel;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bingosoft.components.excel.entity.ExcelInfo;

public class ExcelDefinitionLoaderTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testLoadFromResources() {
		ExcelDefinitionLoader loader = new ExcelDefinitionLoader();
		List<ExcelInfo> list = loader.loadFromResources();
		
		for (ExcelInfo excelInfo : list) {
			System.out.println("excelInfo:" + excelInfo.toString());
		}
		
		assertTrue(list.size() > 0);
	}

	@Test
	public void testGetExcelConfigFiles() {
		ExcelDefinitionLoader loader = new ExcelDefinitionLoader();
		List<File> list = loader.getExcelConfigFiles();
		
		for (File file : list) {
			System.out.println("file:" + file.getPath());
		}
		
		assertTrue(list.size() > 0);
	}
}
