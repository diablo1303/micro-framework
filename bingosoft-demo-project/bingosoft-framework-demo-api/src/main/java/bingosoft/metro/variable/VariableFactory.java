package bingosoft.metro.variable;

/**
 * @author feily
 * @功能描述 环境变量工厂类
 * @创建时间 2018-02-28 13:53:46
 */
public class VariableFactory {

	/**
	 * 获取用户ID
	 * @return 用户ID
	 */
	public static String getUserId(){
		return new UserId().getValue().toString();
	}
	
	/**
	 * 获取租户编码
	 * @return 租户编码
	 */
	public static String getTenantCode(){
		return new TenantCode().getValue().toString();
	}
}
