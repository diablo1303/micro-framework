package bingosoft.metro.variable;

import leap.core.annotation.Bean;
import leap.core.variable.Variable;

/**
 * @author feily
 * @功能描述 用户ID环境变量
 * @创建时间 2018-02-28 13:53:46
 */
@Bean(name = "userId", type = Variable.class)
public class UserId implements Variable {
	
    @Override
    public Object getValue() {
        return "test_user_id";
    }
}