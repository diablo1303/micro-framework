package bingosoft.metro.base.api;

import bingosoft.components.base.api.BaseApiController;
import leap.web.json.JsonSerialize;

/**
 * @author feily
 * @功能描述 Demo模块基础API控制类
 * @创建时间 2018-02-28 13:53:46
 */
@JsonSerialize(dateFormat = "yyyy-MM-dd HH:mm:ss")
public class DemoApiController extends BaseApiController{

}
