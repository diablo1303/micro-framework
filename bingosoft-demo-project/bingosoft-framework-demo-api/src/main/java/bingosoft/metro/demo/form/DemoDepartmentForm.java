package bingosoft.metro.demo.form;

import java.util.ArrayList;
import java.util.List;

import leap.core.doc.annotation.Doc;
import leap.web.form.FormBase;

/**
 * @author feily
 * @功能描述 组织表单类
 * @创建时间 2018-03-02 00:13:39
 */
@Doc("组织")
public class DemoDepartmentForm extends FormBase{

	/**
	 * 部门ID
	 */
	@Doc("部门ID")
	public String departmentId;
	/**
	 * 父部门ID
	 */
	@Doc("父部门ID")
	public String parentDeptId;
	/**
	 * 名称
	 */
	@Doc("名称")
	public String departmentName;
	/**
	 * WBS编码
	 */
	@Doc("WBS编码")
	public String wbsCode;
	/**
	 * 是否启用
	 */
	@Doc("是否启用")
	public Boolean enable;
	/**
	 * 备注
	 */
	@Doc("备注")
	public String remark;

	/**
	 * 获取部门ID
	 * @return 部门ID
	 */
	public String getDepartmentId() {
		return departmentId;
	}
	
	/**
	 * 设置部门ID
	 * @param departmentId 部门ID
	 */
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * 获取父部门ID
	 * @return 父部门ID
	 */
	public String getParentDeptId() {
		return parentDeptId;
	}
	
	/**
	 * 设置父部门ID
	 * @param parentDeptId 父部门ID
	 */
	public void setParentDeptId(String parentDeptId) {
		this.parentDeptId = parentDeptId;
	}

	/**
	 * 获取名称
	 * @return 名称
	 */
	public String getDepartmentName() {
		return departmentName;
	}
	
	/**
	 * 设置名称
	 * @param departmentName 名称
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * 获取WBS编码
	 * @return WBS编码
	 */
	public String getWbsCode() {
		return wbsCode;
	}
	
	/**
	 * 设置WBS编码
	 * @param wbsCode WBS编码
	 */
	public void setWbsCode(String wbsCode) {
		this.wbsCode = wbsCode;
	}

	/**
	 * 获取是否启用
	 * @return 是否启用
	 */
	public Boolean getEnable() {
		return enable;
	}
	
	/**
	 * 设置是否启用
	 * @param enable 是否启用
	 */
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	/**
	 * 获取备注
	 * @return 备注
	 */
	public String getRemark() {
		return remark;
	}
	
	/**
	 * 设置备注
	 * @param remark 备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	/*--------------------------------- 自定义代码部分 begin --------------------------------------*/
	
	/**
	 * 用户列表
	 */
	@Doc("用户列表")
	private List<DemoUserForm> demoUserForms = new ArrayList<DemoUserForm>();

	/**
	 * 获取用户列表
	 * @return
	 */
	public List<DemoUserForm> getDemoUserForms() {
		return demoUserForms;
	}

	/**
	 * 设置用户列表
	 * @param demoUserForms 用户列表
	 */
	public void setDemoUserForms(List<DemoUserForm> demoUserForms) {
		if(demoUserForms != null){
			this.demoUserForms = demoUserForms;
		}
	}

	/*--------------------------------- 自定义代码部分 end --------------------------------------*/
}