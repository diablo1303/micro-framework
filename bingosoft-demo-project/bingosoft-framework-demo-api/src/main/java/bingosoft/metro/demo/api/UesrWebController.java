package bingosoft.metro.demo.api;

import java.util.List;
import java.util.Map;

import bingosoft.components.base.api.BaseApiController;
import bingosoft.components.base.service.PageQueryRequest;
import bingosoft.components.base.service.Result;
import bingosoft.components.base.service.ResultEntity;
import bingosoft.components.base.service.ResultPage;
import bingosoft.metro.demo.form.DemoUserForm;
import bingosoft.metro.demo.iservice.IDemoUserService;
import leap.core.annotation.Inject;
import leap.core.annotation.M;
import leap.core.doc.annotation.Doc;
import leap.core.validation.annotations.Required;
import leap.web.annotation.Path;
import leap.web.annotation.http.DELETE;
import leap.web.annotation.http.GET;
import leap.web.annotation.http.PATCH;
import leap.web.annotation.http.POST;
import leap.web.api.mvc.ApiResponse;
import leap.web.json.JsonSerialize;

/**
 * @author feily
 * @功能描述 用户服务API
 * @创建时间 2018-02-28 13:53:46
 */
@Path("/demoUser")
@JsonSerialize(dateFormat = "yyyy-MM-dd HH:mm:ss")
public class UesrWebController extends BaseApiController{
	
	/**
	 * 用户业务服务接口
	 */
	@Inject @M
	protected IDemoUserService demoUserService;
	
	/**
	 * 构建函数
	 */
	public UesrWebController(){
		this.setLogClass(getClass());
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Doc("分页查询用户")
	/**
	 * 分页查询用户
	 * @param pageQueryRequest 查询选项
	 * @return 用户列表
	 */
	public ApiResponse<ResultPage<List<DemoUserForm>>> pageQueryDemoUsers(@Doc("查询选项") PageQueryRequest pageQueryRequest){
		this.logMethodCalled("pageQueryDemoUsers");
		this.logParamValue("pageQueryRequest", pageQueryRequest);
	
		Map<String, Object> params = null;
		ResultEntity<ResultPage<List<DemoUserForm>>> resultEntity = this.demoUserService.pageQueryDemoUsers(pageQueryRequest, params);
		if(resultEntity.isSuccess()){
			ResultPage<List<DemoUserForm>> resultPage = resultEntity.getBusinessObject();
			return ApiResponse.ok(resultPage);
		}else{
			return ApiResponse.badRequest(resultEntity.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@POST
	@Doc("创建用户")
	/**
	 * 创建用户
	 * @param demoUserForm 用户
	 * @return 操作结果
	 */
	public ApiResponse<String> createDemoUser(@Doc("用户") DemoUserForm demoUserForm){
		this.logMethodCalled("createDemoUser");
		this.logParamValue("demoUserForm", demoUserForm);
		
		Result result = this.demoUserService.createDemoUser(demoUserForm);
		if(result.isSuccess()){
			return ApiResponse.ok(result.getMessage());
		}else{
			return ApiResponse.badRequest(result.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@PATCH("/{userId}")
	@Doc("修改用户")
	/**
	 * 修改用户
	 * @param demoUserForm 用户
	 * @return 操作结果
	 */
	public ApiResponse<String> updateDemoUser(@Doc("用户") DemoUserForm demoUserForm){
		this.logMethodCalled("updateDemoUser");
		this.logParamValue("demoUserForm", demoUserForm);
		
		Result result = this.demoUserService.updateDemoUser(demoUserForm);
		if(result.isSuccess()){
			return ApiResponse.ok(result.getMessage());
		}else{
			return ApiResponse.badRequest(result.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@GET("/{userId}")
	@Doc("获取用户")
	/**
	 * 获取用户
	 * @param userId 用户ID
	 * @return 用户
	 */
	public ApiResponse<DemoUserForm> getDemoUser(@Doc("用户ID") @Required String userId){
		this.logMethodCalled("getDemoUser");
		this.logParamValue("userId", userId);
		
		ResultEntity<DemoUserForm> result = this.demoUserService.getDemoUser(userId);
		if(result.isSuccess()){
			DemoUserForm demoUserForm = result.getBusinessObject();
			return ApiResponse.ok(demoUserForm);
		}else{
			return ApiResponse.badRequest(result.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@DELETE("/{userId}")
	@Doc("删除用户")
	/**
	 * 删除用户
	 * @param userId 用户ID
	 * @return 操作结果
	 */
	public ApiResponse<String> deleteDemoUser(@Doc("用户ID") @Required String userId){
		this.logMethodCalled("deleteDemoUser");
		this.logParamValue("userId", userId);
		
		Result result = this.demoUserService.deleteDemoUser(userId);
		if(result.isSuccess()){
			return ApiResponse.ok(result.getMessage());
		}else{
			return ApiResponse.badRequest(result.getMessage());
		}
	}
}