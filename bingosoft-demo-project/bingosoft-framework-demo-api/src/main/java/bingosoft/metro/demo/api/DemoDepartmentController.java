package bingosoft.metro.demo.api;

import java.util.List;
import java.util.Map;

import bingosoft.components.base.api.BaseApiController;
import bingosoft.components.base.service.PageQueryRequest;
import bingosoft.components.base.service.Result;
import bingosoft.components.base.service.ResultEntity;
import bingosoft.components.base.service.ResultPage;
import bingosoft.metro.demo.form.DemoDepartmentForm;
import bingosoft.metro.demo.iservice.IDemoDepartmentService;
import leap.core.annotation.Inject;
import leap.core.annotation.M;
import leap.core.doc.annotation.Doc;
import leap.core.validation.annotations.Required;
import leap.web.annotation.Path;
import leap.web.annotation.http.DELETE;
import leap.web.annotation.http.GET;
import leap.web.annotation.http.PATCH;
import leap.web.annotation.http.POST;
import leap.web.api.mvc.ApiResponse;
import leap.web.json.JsonSerialize;

/**
 * @author feily
 * @功能描述 组织服务API
 * @创建时间 2018-03-02 02:23:52
 */
@Path("/demoDepartment")
@JsonSerialize(dateFormat = "yyyy-MM-dd HH:mm:ss")
public class DemoDepartmentController extends BaseApiController{
	
	/**
	 * 组织业务服务接口
	 */
	@Inject @M
	protected IDemoDepartmentService demoDepartmentService;
	
	/**
	 * 构建函数
	 */
	public DemoDepartmentController(){
		this.setLogClass(getClass());
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Doc("分页查询组织")
	/**
	 * 分页查询组织
	 * @param pageQueryRequest 查询选项
	 * @return 组织列表
	 */
	public ApiResponse<ResultPage<List<DemoDepartmentForm>>> pageQueryDemoDepartments(@Doc("查询选项") PageQueryRequest pageQueryRequest){
		this.logMethodCalled("pageQueryDemoDepartments");
		this.logParamValue("pageQueryRequest", pageQueryRequest);
		
		Map<String, Object> params = null;
		ResultEntity<ResultPage<List<DemoDepartmentForm>>> resultEntity = this.demoDepartmentService.pageQueryDemoDepartments(pageQueryRequest, params);
		if(resultEntity.isSuccess()){
			ResultPage<List<DemoDepartmentForm>> resultPage = resultEntity.getBusinessObject();
			return ApiResponse.ok(resultPage);
		}else{
			return ApiResponse.badRequest(resultEntity.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@POST
	@Doc("创建组织")
	/**
	 * 创建组织
	 * @param demoDepartmentForm 组织
	 * @return 操作结果
	 */
	public ApiResponse<String> createDemoDepartment(@Doc("组织") DemoDepartmentForm demoDepartmentForm){
		this.logMethodCalled("createDemoDepartment");
		this.logParamValue("demoDepartmentForm", demoDepartmentForm);
		
		Result result = this.demoDepartmentService.createDemoDepartment(demoDepartmentForm);
		if(result.isSuccess()){
			return ApiResponse.ok(result.getMessage());
		}else{
			return ApiResponse.badRequest(result.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@PATCH("/{DepartmentId}")
	@Doc("修改组织")
	/**
	 * 修改组织
	 * @param demoDepartmentForm 组织
	 * @return 操作结果
	 */
	public ApiResponse<String> updateDemoDepartment(@Doc("组织") DemoDepartmentForm demoDepartmentForm){
		this.logMethodCalled("updateDemoDepartment");
		this.logParamValue("demoDepartmentForm", demoDepartmentForm);
		
		Result result = this.demoDepartmentService.updateDemoDepartment(demoDepartmentForm);
		if(result.isSuccess()){
			return ApiResponse.ok(result.getMessage());
		}else{
			return ApiResponse.badRequest(result.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@GET("/{DepartmentId}")
	@Doc("获取组织")
	/**
	 * 获取组织
	 * @param DepartmentId 组织ID
	 * @return 组织
	 */
	public ApiResponse<DemoDepartmentForm> getDemoDepartment(@Doc("组织ID") @Required String DepartmentId){
		this.logMethodCalled("getDemoDepartment");
		this.logParamValue("DepartmentId", DepartmentId);
		
		ResultEntity<DemoDepartmentForm> result = this.demoDepartmentService.getDemoDepartment(DepartmentId);
		if(result.isSuccess()){
			DemoDepartmentForm demoDepartmentForm = result.getBusinessObject();
			return ApiResponse.ok(demoDepartmentForm);
		}else{
			return ApiResponse.badRequest(result.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	@DELETE("/{DepartmentId}")
	@Doc("删除组织")
	/**
	 * 删除组织
	 * @param DepartmentId 组织ID
	 * @return 操作结果
	 */
	public ApiResponse<String> deleteDemoDepartment(@Doc("组织ID") @Required String DepartmentId){
		this.logMethodCalled("deleteDemoDepartment");
		this.logParamValue("DepartmentId", DepartmentId);
		
		Result result = this.demoDepartmentService.deleteDemoDepartment(DepartmentId);
		if(result.isSuccess()){
			return ApiResponse.ok(result.getMessage());
		}else{
			return ApiResponse.badRequest(result.getMessage());
		}
	}
}