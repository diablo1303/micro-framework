package bingosoft.metro.demo.form;

import java.util.Date;

import bingosoft.components.base.form.ActionForm;
import leap.core.doc.annotation.Doc;

/**
 * @author feily
 * @功能描述 用户表单类
 * @创建时间 2018-02-28 13:53:46
 */
@Doc("用户")
public class DemoUserForm extends ActionForm{

	/**
	 * 用户ID
	 */
	@Doc("用户ID")
	private String userId;
	/**
	 * 部门ID
	 */
	@Doc("部门ID")
	private String departmentId;
	/**
	 * 姓名
	 */
	@Doc("姓名")
	private String userName;
	/**
	 * 生日
	 */
	@Doc("生日")
	private Date birthday;
	/**
	 * 年龄
	 */
	@Doc("年龄")
	private Integer age;
	/**
	 * 体重
	 */
	@Doc("体重")
	private Double weight;
	/**
	 * 是否启用
	 */
	@Doc("是否启用")
	private Boolean enable;
	/**
	 * 备注
	 */
	@Doc("备注")
	private String remark;


	/**
	 * 获取用户ID
	 * @return 用户ID
	 */
	public String getUserId() {
		return userId;
	}
	
	/**
	 * 设置用户ID
	 * @param userId 用户ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 获取部门ID
	 * @return 部门ID
	 */
	public String getDepartmentId() {
		return departmentId;
	}
	
	/**
	 * 设置部门ID
	 * @param departmentId 部门ID
	 */
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * 获取姓名
	 * @return 姓名
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * 设置姓名
	 * @param userName 姓名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 获取生日
	 * @return 生日
	 */
	public Date getBirthday() {
		return birthday;
	}
	
	/**
	 * 设置生日
	 * @param birthday 生日
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * 获取年龄
	 * @return 年龄
	 */
	public Integer getAge() {
		return age;
	}
	
	/**
	 * 设置年龄
	 * @param age 年龄
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 * 获取体重
	 * @return 体重
	 */
	public Double getWeight() {
		return weight;
	}
	
	/**
	 * 设置体重
	 * @param weight 体重
	 */
	public void setWeight(Double weight) {
		this.weight = weight;
	}

	/**
	 * 获取是否启用
	 * @return 是否启用
	 */
	public Boolean getEnable() {
		return enable;
	}
	
	/**
	 * 设置是否启用
	 * @param enable 是否启用
	 */
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	/**
	 * 获取备注
	 * @return 备注
	 */
	public String getRemark() {
		return remark;
	}
	
	/**
	 * 设置备注
	 * @param remark 备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

}