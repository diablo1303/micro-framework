package bingosoft.metro.demo.model;

import java.util.Date;

import leap.lang.enums.Bool;
import leap.orm.annotation.Column;
import leap.orm.annotation.Table;
import leap.orm.model.Model;

/**
 * @author feily
 * @功能描述 组织模型类
 * @创建时间 2018-02-28 13:53:46
 */
@Table("demo_department")
public class DemoDepartmentModel extends Model{

	/**
	 * 部门ID
	 */
	private String departmentId;
	/**
	 * 父部门ID
	 */
	private String parentDeptId;
	/**
	 * 名称
	 */
	private String departmentName;
	/**
	 * WBS编码
	 */
	private String wbsCode;
	/**
	 * 是否启用
	 */
	private Boolean enable;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 创建时间
	 */
	@Column(update = Bool.FALSE)
	private Date createTime;
	/**
	 * 创建用户ID
	 */
	@Column(update = Bool.FALSE)
	private String createUserId;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	/**
	 * 修改用户ID
	 */
	private String updateUserId;
	/**
	 * 所属租户编码
	 */
	private String tenantCode;


	/**
	 * 获取部门ID
	 * @return 部门ID
	 */
	public String getDepartmentId() {
		return departmentId;
	}
	
	/**
	 * 设置部门ID
	 * @param departmentId 部门ID
	 */
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * 获取父部门ID
	 * @return 父部门ID
	 */
	public String getParentDeptId() {
		return parentDeptId;
	}
	
	/**
	 * 设置父部门ID
	 * @param parentDeptId 父部门ID
	 */
	public void setParentDeptId(String parentDeptId) {
		this.parentDeptId = parentDeptId;
	}

	/**
	 * 获取名称
	 * @return 名称
	 */
	public String getDepartmentName() {
		return departmentName;
	}
	
	/**
	 * 设置名称
	 * @param departmentName 名称
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * 获取WBS编码
	 * @return WBS编码
	 */
	public String getWbsCode() {
		return wbsCode;
	}
	
	/**
	 * 设置WBS编码
	 * @param wbsCode WBS编码
	 */
	public void setWbsCode(String wbsCode) {
		this.wbsCode = wbsCode;
	}

	/**
	 * 获取是否启用
	 * @return 是否启用
	 */
	public Boolean getEnable() {
		return enable;
	}
	
	/**
	 * 设置是否启用
	 * @param enable 是否启用
	 */
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	/**
	 * 获取备注
	 * @return 备注
	 */
	public String getRemark() {
		return remark;
	}
	
	/**
	 * 设置备注
	 * @param remark 备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * 获取创建时间
	 * @return 创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	
	/**
	 * 设置创建时间
	 * @param createTime 创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 获取创建用户ID
	 * @return 创建用户ID
	 */
	public String getCreateUserId() {
		return createUserId;
	}
	
	/**
	 * 设置创建用户ID
	 * @param createUserId 创建用户ID
	 */
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	/**
	 * 获取修改时间
	 * @return 修改时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	
	/**
	 * 设置修改时间
	 * @param updateTime 修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 获取修改用户ID
	 * @return 修改用户ID
	 */
	public String getUpdateUserId() {
		return updateUserId;
	}
	
	/**
	 * 设置修改用户ID
	 * @param updateUserId 修改用户ID
	 */
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * 获取所属租户编码
	 * @return 所属租户编码
	 */
	public String getTenantCode() {
		return tenantCode;
	}
	
	/**
	 * 设置所属租户编码
	 * @param tenantCode 所属租户编码
	 */
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
}
