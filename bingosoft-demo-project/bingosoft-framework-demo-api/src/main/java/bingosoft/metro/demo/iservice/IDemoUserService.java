package bingosoft.metro.demo.iservice;

import java.util.List;
import java.util.Map;

import bingosoft.components.base.service.PageQueryRequest;
import bingosoft.components.base.service.Result;
import bingosoft.components.base.service.ResultEntity;
import bingosoft.components.base.service.ResultPage;
import bingosoft.metro.demo.form.DemoUserForm;
import bingosoft.metro.demo.model.DemoUserModel;

/**
 * @author feily
 * @功能描述 用户业务服务接口类
 * @创建时间 2018-02-28 13:53:46
 */
public interface IDemoUserService {

	/**
	 * 查询用户
	 * @param pageQueryRequest 查询条件
	 * @return 查询结果
	 */
	ResultEntity<List<DemoUserForm>> queryDemoUsers(PageQueryRequest pageQueryRequest, Map<String, Object> params);
	
	/**
	 * 分页查询用户
	 * @param pageQueryRequest 分页查询条件
	 * @return 分页查询结果
	 */
	ResultEntity<ResultPage<List<DemoUserForm>>> pageQueryDemoUsers(PageQueryRequest pageQueryRequest, Map<String, Object> params);
	
	/**
	 * 创建用户
	 * @param demoUserForm 用户对象
	 * @return 操作结果
	 */
	Result createDemoUser(DemoUserForm demoUserForm);
	
	/**
	 * 修改用户
	 * @param demoUserForm 用户对象
	 * @return 操作结果
	 */
	Result updateDemoUser(DemoUserForm demoUserForm);
	
	/**
	 * 获取用户
	 * @param userId 用户ID
	 * @return 
	 */
	ResultEntity<DemoUserForm> getDemoUser(String userId);
	
	/**
	 * 获取用户
	 * @param userId 用户ID
	 * @return 
	 */
	ResultEntity<DemoUserModel> getDemoUserModel(String userId);

	/**
	 * 删除用户
	 * @param userId 用户ID
	 * @return 操作结果
	 */
	Result deleteDemoUser(String userId);
}