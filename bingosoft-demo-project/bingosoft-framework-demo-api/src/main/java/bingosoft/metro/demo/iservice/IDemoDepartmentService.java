package bingosoft.metro.demo.iservice;

import java.util.List;
import java.util.Map;

import bingosoft.components.base.service.PageQueryRequest;
import bingosoft.components.base.service.Result;
import bingosoft.components.base.service.ResultEntity;
import bingosoft.components.base.service.ResultPage;
import bingosoft.metro.demo.form.DemoDepartmentForm;
import bingosoft.metro.demo.model.DemoDepartmentModel;

/**
 * @author feily
 * @功能描述 组织业务服务接口类
 * @创建时间 2018-03-02 02:23:52
 */
public interface IDemoDepartmentService {

	/**
	 * 查询组织
	 * @param pageQueryRequest 查询条件
	 * @return 查询结果
	 */
	ResultEntity<List<DemoDepartmentForm>> queryDemoDepartments(PageQueryRequest pageQueryRequest, Map<String, Object> params);
	
	/**
	 * 分页查询组织
	 * @param pageQueryRequest 分页查询条件
	 * @return 分页查询结果
	 */
	ResultEntity<ResultPage<List<DemoDepartmentForm>>> pageQueryDemoDepartments(PageQueryRequest pageQueryRequest, Map<String, Object> params);
	
	/**
	 * 创建组织
	 * @param demoDepartmentForm 组织对象
	 * @return 操作结果
	 */
	Result createDemoDepartment(DemoDepartmentForm demoDepartmentForm);
	
	/**
	 * 修改组织
	 * @param demoDepartmentForm 组织对象
	 * @return 操作结果
	 */
	Result updateDemoDepartment(DemoDepartmentForm demoDepartmentForm);
	
	/**
	 * 获取组织
	 * @param departmentId 组织ID
	 * @return 组织
	 */
	ResultEntity<DemoDepartmentForm> getDemoDepartment(String departmentId);

	/**
	 * 获取组织
	 * @param departmentId 组织ID
	 * @return 组织
	 */
	ResultEntity<DemoDepartmentModel> getDemoDepartmentModel(String departmentId);

	/**
	 * 删除组织
	 * @param departmentId 组织ID
	 * @return 操作结果
	 */
	Result deleteDemoDepartment(String departmentId);
}