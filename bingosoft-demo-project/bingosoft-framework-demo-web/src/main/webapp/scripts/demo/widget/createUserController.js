/**
 * 日期：2018-04-20
 * 作者：Chenzx
 * 描述：创建用户页面控制类
 */
var _className= "createUserController",
	widgets = {}, // 保存页面初始化后的全部组件对象
	_devUrl = Global.contextPath + "/scripts/demo/form/data/";

/**
 * 加载页面信息
 * @return {[type]} [description]
 */
var loadPageInfo = function(){
    // 初始化页面组件
    _initWidget();
    // 绑定按钮事件
    _bindBtnEvent();
}

/**
 * 初始化组件
 * @return {[type]} [description]
 */
var _initWidget = function(){
    // 页面组件全部初始化
//    widgets = wui.init();
    // console.info(widgets);
//	console.info(Global.staticContext);
	var widgetParamsSetting = {
		"type": {
			"loadUrl": _devUrl + "_queryType.json",
			"maxNumber": 3
		},
		"publishTime": {
			"startDate": wui.getDate(0) 
		},
//		"publishDepart": {
//			"loadUrl": _devUrl + "_queryDeparts.json",
//			"name": "departmentId",
//			"text": "departmentName",
//			"maxNumber": 1
//		},
		"actor": {
			"treeTitle": "部门导航",
			"treeUrl": _devUrl + "_queryDepartsForNav.json",
			"tabUrl": Global.contextPath + "/demo/testList/testList3.html"
		},
		"uploadVideo": {
//			"url": "http://111.230.47.180:7070/Upload.aspx",
			"url": Global.staticContext + "/base/upload/exec",
			"maxNumber": 1,
			"prmNames": {
				"data": "businessObject"
			},
			"onSuccess": function(file, response){
				if(response.isSuccess){
					wui.notice(JSON.stringify(response.businessObject));
				}else{
					wui.errorNotice(response.message);
				}
			},
			"onFail": function(file, response){
				wui.warnNotice(response.message);
			}
		},
		"dataUpload": {
//			"url": "http://111.230.47.180:7070/Upload.aspx",
			"url": Global.staticContext + "/base/upload/exec",
			"maxNumber": 5,
			"prmNames": {
				"data": "businessObject"
			},
			"onSuccess": function(file, response){
				if(response.isSuccess){
					wui.notice(JSON.stringify(response.businessObject));
				}else{
					wui.errorNotice(response.message);
				}
			},
			"onFail": function(file, response){
				wui.warnNotice(response.message);
			}
		}
	}
	
	widgets["type"] = wui.tagBox("type", widgetParamsSetting["type"]);
	widgets["publishTime"] = wui.date("publishTime", widgetParamsSetting["publishTime"]);
//	widgets["publishDepart"] = wui.selectBox("publishDepart", widgetParamsSetting["publishDepart"]);
	widgets["actor"] = wui.chooseBox("actor", widgetParamsSetting["actor"]);
	widgets["uploadVideo"] = wui.file("uploadVideo", widgetParamsSetting["uploadVideo"]);
	widgets["dataUpload"] = wui.file("dataUpload", widgetParamsSetting["dataUpload"]);
}

/**
 * 绑定按钮事件
 * @return {[type]} [description]
 */
var _bindBtnEvent = function(){
    // 提交按钮事件
    $("#btnSubmit").on("click", function(){
        var formValue = wui.getForm("#widgetForm");
//        var dataHtml = wui.dataToCode(formValue);
        var html = '';
        html += '<div class="code-temp">';
        html += JSON.stringify(formValue);
//        html += '    <figure class="highlight">';
//        html += '        <pre data-code="widget">' + JSON.stringify(formValue) + '</pre>';
//        html += '    </figure>';
        html += '</div>';
        wui.openHtml({
            "title": "表单取值结果",
            "content": html
        });
    })

    // 重置按钮事件
    $("#btnReset").on("click", function(){
        wui.emptyForm();
    })
}