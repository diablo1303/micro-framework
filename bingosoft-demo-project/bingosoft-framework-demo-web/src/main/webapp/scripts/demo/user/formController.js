/**
 * 日期：2018-04-14
 * 作者：feily
 * 描述：用户表单页面控制类
 */

var _className = "userFormController";

/**
 * 加载页面信息
 */
function loadPageInfo(){
	wui.logMethodCalled("loadPageInfo", _className);
	
	// 初始化WUI组件
	_initWui();
    
	// 加载用户数据
	_loadUserData();
}

/**
 * 保存或修改用户信息
 */
function saveOrUpdateUser(){
	wui.logMethodCalled("saveOrUpdateUser", _className);
	
	// 验证表单数据
	if(_validateForm()){       	    		
		var user = wui.getForm();
		var success = function(message){
			wui.closeModalDialog(message);
	    };
	 	var fail = function(result){
	    	wui.errorNotice(result.message);
	    };
		
		// 创建或修改用户信息
		if(action == "add"){
			userService.createUser(user, success, fail);
		}else if(action == "edit"){
			user.userId = userId;
			userService.updateUser(user, success, fail);
		}
	}else{
		wui.warnNotice("表单数据校验未通过，请重新检查数据！");
	}
}
	
/**
 * 验证表单数据
 */
function _validateForm(){
	wui.logMethodCalled("_validateForm", _className);
	
	var validateConfigs = userService.getValidateConfigs(); // 验证配置
	var validate = wui.validate("#form", validateConfigs);
	return validate.valid();
}

/**
 * 初始化WUI组件
 */
function _initWui(){
	wui.logMethodCalled("_initWui", _className);
	
	$("[widget-init]").each(function (index, element) {
        var widgetName = $(element).attr("widget-init");
        if (!wui[widgetName]) {
            alert(widgetName + "组件不存在");
            return;
        }
        
        var widgetId = $(element).attr("id");
        var widgetConfigs = userService.getFormWidgetConfigs(); // wui配置信息
        widgets[widgetId] = wui[widgetName](widgetId, widgetConfigs[widgetId]);
    });
}

/**
 * 加载用户数据
 */
function _loadUserData(){
	wui.logMethodCalled("_loadUserData", _className);

	if(action == "view" || action == "edit"){
		// 禁用表单
		if(action == "view"){
    		wui.disabled();
            $("#btnSubmit").hide();
            $("#btnCancel").text("关闭");
		}
		
		// 加载用户信息
		if(userId){
            var success = function(user){
            	wui.fillForm(user);
            };
            userService.getUser(userId, success);
		}
	}
}
