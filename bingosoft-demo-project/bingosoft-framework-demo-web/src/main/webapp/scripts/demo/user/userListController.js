/**
 * 日期：2018-04-14
 * 作者：feily
 * 描述：用户列表页面控制类
 */

var _className = "userListController";

/**
 * 网格加载完成事件
 */
function gridComplete(){
	wui.logMethodCalled("gridComplete", _className);
	
    // Grid按钮
    $("#grid-table a[_action]").each(function(i){
        var action = $(this).attr("_action");
        var dataId = $(this).attr("_id");
        
        $(this).click(function (ev){
            var ev = ev || windwo.ev;
            wui.stopPropagation(ev);

            wui.logMethodCalled("grid-" + action + ".click", _className);
            openForm(action, dataId);
        });
    });
}

/**
 * 弹出表单
 * @param id: ID
 * @param action：操作类型
 */
function openForm(action,id) {
    wui.logMethodCalled("openForm", _className);
    wui.logParamValue("action", action, _className);
    wui.logParamValue("id", id, _className);

    id = id || "";
    action = action || "add";

    var title = "用户信息";
    switch(action){
        case "add": "新增" + title;
            break;	
        case "edit": "编辑"+ title;
            break;
        case "view": "查看" + title;
        	break;
        case "delete":
            wui.deleteDataByIds(id, deleteData);
            return;
    }

    var data = {userId: id, action: action};
    var url = wui.buildUrl('/bingosoft-framework-demo-web/demo/user/form.html', data);
    wui.openModalDialog(url, _refresh, '800px', '550px', title);
};

/**
 * 根据ID删除数据
 * @param ids
 */
function deleteData(ids){
    wui.logMethodCalled("_deleteData", _className);
    wui.logParamValue("ids", ids, _className);
    
    var success = _refresh;
    userService.deleteUser(ids, success);
}

/**
 * 刷新表单
 * @param message string 消息
 */
function _refresh(message){
    wui.logMethodCalled("_refresh", _className);
    wui.logParamValue("message", message, _className);
    
    wui.successNotice(message);
    uiGrid.resetGrid();
}