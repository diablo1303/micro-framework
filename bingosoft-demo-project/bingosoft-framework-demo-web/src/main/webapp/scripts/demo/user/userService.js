/**
 * 日期：2018-04-14
 * 作者：feily
 * 描述：用户业务服务类
 */
var userService = (function () {
	
	var _isDev = true;
	var _className = "userService";
	
	var _url = "";
	var _devUrl = Global.contextPath + "/scripts/demo/user/data/";
	
    /**
     * 获取用户列表配置信息
     * @param gridComplete function: 网格加载完成回调函数
     * @return json grid配置信息
     */
	var _getGridConfig = function(gridComplete){
		wui.logMethodCalled("_getGridConfig" ,_className);
		
		var url = _url;
		if(_isDev){
        	url = _devUrl + "_queryUsers.json";
        }
		
		/**
		 * 操作列格式化
		 * @param cellValue: 当前列的值
		 * @param options：配置参数
		 * @param rowObject：当前行的值
		 */
		var _formatOperation = function(cellValue, options, rowObject) {
		    var data = { id: cellValue };
		    return template('operationTemplate', data);
		};
		
		var cols = [    
            { label: '姓名', name: 'userName', sortable: true, search: true, sopt: 'cn' },
            { label: '部门', name: 'departmentName', sortable: true, search: true, sopt: 'cn' },
            { label: '生日', name: 'birthday', formatter: 'date', sortable: true, search: true, sopt: 'le', width:90 },
            { label: '年龄', name: 'age', formatter: 'int', sortable: true, search: true, sopt: 'le', width:60, width:60 },
            { label: '体重', name: 'weight', formatter: 'number', search: true, sopt: 'le', width:60 },
            { label: '状态', name: 'enableText',  search: true, sopt: 'eq', width:60},
            { label: '创建时间', name: 'createTime', width:120},
            { label: '操作', name: 'userId', key: true, width:100, formatter: _formatOperation},
        ]
		
		var config = {
            "url": url,
            "gridToolbar": ".w-grid-toolbar",
            "colModel": cols,
            "gridComplete" : gridComplete
        };
		return config;
	};
	
    /**
     * 获取用户表单wui配置信息
     * @return json 用户表单wui配置信息
     */
	var _getFormWidgetConfigs = function(){
        var configs = {
        	"departmentId": {
        		"loadUrl": _devUrl + "_queryDeparts.json",
                "maxNumber": 1,
                "name": "departmentId",
                "text": "departmentName",
                "prmNames": {
                    "data": null
                }
        	}
        };
        return configs;
	};
	
    /**
     * 获取用户表单验证配置信息
     * @return json 用户表单wui配置信息
     */
	var _getValidateConfigs = function(){
        var configs = {
                rules: {
//                    "userName": {
//                        required: true,
//                        onlyDoubleChars: true
//                    },
//                    "birthday": "required",
//                    "age": "required",
//                    "weight": "required",
//                    "enable": "required"
                }
    	};
        return configs;
	};
	
    /**
     * 获取用户信息
     * @param userId string: 用户ID
	 * @param success function: 操作成功回调函数
	 * @param fail function: 操作失败回调函数
     */
    var _getUser = function (userId, success, fail) {
        wui.logMethodCalled("_getUser" ,_className);

        if(!userId){
        	alert("参数userId不能为空！");
        	return;
        }
        
        var url = _url + "/" + userId;
		if(_isDev){
        	url = _devUrl + "_getUser_$userId$.json";
        	
        	var userId = "zhangsan";
        	var queryParam = wui.getQueryString();
        	if(queryParam.userId){
        		userId = queryParam.userId;
        	}
        	url = url.replace("$userId$", userId);
        }
       
        var param = {
        	url: url,
        	successCallback: success,
        	errorCallback: fail
        }
        wui.getAjax(param);
    };
    
    /**
     * 创建用户信息
     * @param param 参数，json对象
	 *        param.xxx: 用户信息
	 * @param success function: 操作成功回调函数
	 * @param fail function: 操作失败回调函数
     */
    var _createUser= function (user, success, fail) {
        wui.logMethodCalled("_createUser" ,_className);
 
        var url = _url;
		if(_isDev){
        	url = _devUrl + "_createOrUpdateUser.txt";
        }
		
        var param = {
        	url: url,
        	data: JSON.stringify(user),
        	dataType : "text", // 返回类型为文本
        	successCallback: success,
        	errorCallback: fail
        }
        wui.ajax(param);
    };
    
    /**
     * 修改用户信息
     * @param param 参数，json对象
	 *   param.xxx: 用户信息
	 * @param success function: 操作成功回调函数
	 * @param fail function: 操作失败回调函数
     */
    var _updateUser = function (user, success, fail) {
        wui.logMethodCalled("_updateUser" ,_className);
        
        if(!user.userId){
        	alert("参数user.userId不能为空！");
        	return;
        }
        
        var url = _url + "/" + user.userId;
		if(_isDev){
        	url = _devUrl + "_createOrUpdateUser.txt";
        }
	
        var param = {
        	url: url,
        	data: JSON.stringify(user),
        	dataType : "text", // 返回类型为文本
        	successCallback: success,
        	errorCallback: fail
        }
        
        if(_isDev){
        	wui.ajax(param);
        	return;
        }
        wui.patchAjax(param);
    };
    
    /**
     * 删除用户信息
     * @param userId string: 用户ID
	  *@param success function: 操作成功回调函数
	 * @param fail function: 操作失败回调函数
     */
    var _deleteUser = function (userId, success, fail) {
        wui.logMethodCalled("_deleteUser" ,_className);
        
        if(!userId){
        	alert("参数userId不能为空！");
        	return;
        }
        
        var url = _url + "/" + userId;
		if(_isDev){
        	url = _devUrl + "_deleteUser.txt";
            var param = {
            	url: url,
            	dataType : "text", // 返回类型为文本
            	successCallback: success,
            	errorCallback: fail
            }
            wui.ajax(param);
            return;
        }
		
        var param = {
        	url: url,
        	successCallback: success,
        	errorCallback: fail
        }
        wui.deleteAjax(param);
    };
    
    /**
     * 导入用户信息
     * @param files json: excel文件
	 * @success function: 操作成功回调函数
	 * @fail function: 操作失败回调函数
     */
    var _importUser = function (files, success, fail) {
        wui.logMethodCalled("_importUser" ,_className);
        
        var data = {method: "importUser", importFilePath: files[0].filePath};
        var param = {
        	url: _url,
        	data: data,
        	successCallback: success,
        	errorCallback: fail
        }
        wui.ajax(param);
    };
    
    // 对外调用方法
    return {

    	// 获取用户列表配置信息
    	getGridConfig : _getGridConfig,
    	
    	// 获取用户表单wui配置信息
    	getFormWidgetConfigs : _getFormWidgetConfigs,
    	
        // 获取用户表单验证配置信息
        getValidateConfigs : _getValidateConfigs,
    	
        // 获取用户信息
        getUser: _getUser,
        
        // 创建用户信息
        createUser: _createUser,
        
        // 修改用户信息
        updateUser: _updateUser,
        
        // 删除用户信息
        deleteUser: _deleteUser,
        
        // 导入用户信息
        importUser: _importUser
    }
    
})();
