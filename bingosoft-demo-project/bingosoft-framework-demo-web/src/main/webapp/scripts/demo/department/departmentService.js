/**
* 日期：2018-04-20
* 作者：Chenzx
* 描述：部门组织业务服务类
*/
var departmentService = (function () {
	
	var _className = "departmentService";
	var _isDev = wui.isDev;
	var _url = "";
	var _devUrl = Global.contextPath + "/scripts/demo/department/data/";
	
	/**
     * 获取列表配置参数
     * @param  {function} gridComplete [列表加载完成执行方法]
     * @return {JSON} 用户列表组件配置参数
     */
    var _getUserGridConfig = function(departmentId, gridComplete){
        wui.logMethodCalled("_getUserGridConfig" ,_className);

        var url = _url;
        if(_isDev){
            url = _devUrl + "_queryUsersByDepartmentId_" + departmentId + ".json";
        }
        /**
         * 操作列格式化
         * @param cellValue: 当前列的值
         * @param options：配置参数
         * @param rowObject：当前行的值
         */
        var _formatOperation = function (cellValue, options, rowObject) {
            var data = { id: cellValue };
            return template('operationTemplate', data);
        }

        var config = {
            "url": url,
            "gridToolbar": ".w-grid-toolbar",
            "colModel": [
                { label: '操作', name: 'id', key: true, formatter: _formatOperation},
                { label: 'ID', name: 'id', search: true, sopt: 'cn', hidden: true },
                { label: '姓名', name: 'name', sortable: false, search: true, sopt: 'cn' },
                { label: '生日', name: 'birthday', formatter: 'date', sortable: true, search: true, sopt: 'le' },
                { label: '体重', name: 'weight', formatter: 'number', search: true, sopt: 'le' },
                { label: '是否启用', name: 'enable', formatter: 'checkbox', search: true, sopt: 'eq' },
                { label: '备注', name: 'remark', align: 'left' }
            ],
            "gridComplete" : function(){
                gridComplete && typeof(gridComplete) === "function" && gridComplete();
            }
        }
        return config;
    };

    /**
     * 获取部门信息
     * @param departmentId string: 用户ID
     * @success function: 操作成功回调函数
     * @fail function: 操作失败回调函数
     */
    var _getDepartsTreeConfig = function (activate, contextAction) {
        wui.logMethodCalled("_getDepartsTreeConfig" ,_className);

        var url = _url;
        if(_isDev){
            url = _devUrl + "_queryDeparts.json";
        }
        // console.error(theUrl);
        
        var config = {
            "loadDataUrl": url,
            "activate": function(event, data){
                var node = data.node;
                // console.info(node);
                var id = node.key,
                    name = node.title;
                activate && typeof(activate) === "function" && activate(id, name);
                // $("#nodeTitle").text(name);
                // if(uiGrid){
                //     uiGrid.searchGrid({
                //         "departmentId": id
                //     });
                // }
            },
            "contextMenu": {
                menu: {
                    "add": { "name": "新增", "icon": "add" },
                    "view": { "name": "查看", "icon": "fa-search" },
                    "edit": { "name": "编辑", "icon": "edit" },
                    "del": { "name": "删除", "icon": "delete" }
                },    // 菜单列表
                actions: function (node, action, options) {
                    
                    contextAction && typeof(contextAction) === "function" && contextAction(node, action, options);
                }    // 点击菜单执行方法
            }
        };

        return config;
    };

    /**
     * 根据部门id搜索对应的员工信息
     * @param  {Object} userGrid     [用户列表组件对象]
     * @param  {String} departmentId [部门id]
     * @return NULL
     */
    var _searchUsersByDepartmentId = function(userGrid, departmentId){
        wui.logMethodCalled("_searchUsersByDepartmentId" ,_className);

        var searchUrl = _url;
        if(_isDev){
            if(!departmentId || !(departmentId == "0101" || departmentId == "010101" || departmentId == "010102" || departmentId == "010103")){
                departmentId = "01";
            }
            searchUrl = _devUrl + "_queryUsersByDepartmentId_" + departmentId + ".json";

            userGrid.searchGrid({}, searchUrl);
        }else{
            userGrid.searchGrid({"departmentId": departmentId});
        }
    }
    
    // 对外调用方法
    return {
        // 获取列表配置参数
        getUserGridConfig: _getUserGridConfig,

        // 获取部门树结构配置参数
        getDepartsTreeConfig: _getDepartsTreeConfig,

        // 根据部门id搜索对应的员工信息
        searchUsersByDepartmentId: _searchUsersByDepartmentId
    }
})();
