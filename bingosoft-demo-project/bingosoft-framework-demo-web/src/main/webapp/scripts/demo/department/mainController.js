/**
 * 日期：2018-04-20
 * 作者：Chenzx
 * 描述：部门管理页面控制类
 */
var _treeDepartsId = "departsTree", // 部门导航ID
    _gridUsersId = "usersGrid", // 人员列表ID
    _gridUserInfo = null;  // 组件grid对象

/**
 * 加载页面信息
 * @return null
 */
var loadPageInfo = function(){
    // 加载树形结构的部门信息
    _loadDepartsInfo(_loadUsersInfoByDepartmentId);
}

/**
 * 加载树形结构的部门信息
 * @return null
 */
var _loadDepartsInfo = function(callback){

    /**
     * 树节点点击触发事件
     * @param  {String} key   [节点ID]
     * @param  {String} title [节点文本]
     * @return null
     */
    var _activate = function(key, title){

        $("#nodeTitle").text(title);
        callback && typeof(callback) == "function" && callback(key);
    };

    // 获取部门树结构配置
    var treeConfig = departmentService.getDepartsTreeConfig(_activate, _contextAction);
    // 组织机构树初始化
    var uiTree = wui.tree(_treeDepartsId, treeConfig);
}

/**
 * 加载部门员工信息列表
 * @return {[type]} [description]
 */
var _loadUsersInfoByDepartmentId = function(departmentId){
    if(_gridUserInfo){
        // 已经初始化组件,可以搜索表格数据
    	departmentService.searchUsersByDepartmentId(_gridUserInfo, departmentId);
    }else{
        // 获取默认组件配置
        var userGridConfig = departmentService.getUserGridConfig(departmentId, _gridComplete);
        // 初始化组件
        _gridUserInfo = wui.grid(_gridUsersId, userGridConfig);
    }
}

/**
 * 刷新
 * @param json serviceResult
 */
var _refresh = function(serviceResult){
    wui.logMethodCalled("_refresh");

    wui.successNotice(serviceResult);
    _gridUserInfo.resetGrid();
    
    // if(serviceResult.isSuccess){
    //     wui.successNotice(serviceResult.message);
    //     _gridUserInfo.resetGrid();
    // }else{
    //     wui.errorNotice(serviceResult.message);
    // }
}

/**
 * 根据ID删除数据
 * @param  {String} ids [删除数据ID]
 * @return NULL
 */
var _deleteData = function(ids){
    wui.logMethodCalled("_deleteData");
    // console.info("删除成功");
    var success = _refresh,
        fail = function(res){
            wui.errorNotice(res);
        };
    userService.deleteUser(ids, success, fail);
}

/**
 * 弹出表单
 * @param  {[type]} action [操作类型:"add"/"view"/"edit"/"delete",默认是"add"]
 * @param  {[type]} id     [操作数据行id]
 * @return {[type]}        [description]
 */
var _openForm = function(action,id) {
    wui.logMethodCalled("_openForm");
    wui.logParamValue("action",action);
    wui.logParamValue("id",id);

    id = id || "";
    action = action || "add";

    var title = "用户信息";
    switch(action){
        case "add": title += "新增";
            break;
        case "view": title += "查看";
            break;
        case "edit": title += "编辑";
            break;
        case "delete":
            wui.deleteDataByIds(id, _deleteData);
            return;
    }

    var data = {userId: id, action:action};
    var url = wui.buildUrl('/bingosoft-framework-demo-web/demo/user/form.html', data);
    wui.openModalDialog(url, _refresh, '800px', '550px', title);
};

/**
 * Grid加载完成执行方法
 * @return {[type]} [description]
 */
var _gridComplete = function(){
    // Grid按钮
    $("#" + _gridUsersId + " a[action]").each(function(i){
        
        var action = $(this).attr("action");
        var rowid = $(this).attr("_id");
        
        $(this).click(function (ev){
            var ev = ev || windwo.ev;
            wui.stopPropagation(ev);
            _openForm(action, rowid);
        });
    });
};

/**
 * 右键菜单点击触发事件
 * @param  {[type]} node    [右键菜单选中菜单节点对象]
 * @param  {[type]} action  [右键菜单选中菜单节点操作]
 * @param  {[type]} options [右键菜单选中菜单节点配置]
 * @return {[type]}         [description]
 */
var _contextAction = function(node, action){
    wui.logMessage("选中操作: '" + action + "' ,操作节点: " + node + ".");
};
