/**
 * 日期：2018-04-20
 * 作者：Chenzx
 * 描述：chooseBox弹出公共页面控制类
 */
var _className = "chooseBoxPopupController";
// 获取链接信息
var returnValue = wui.getQueryString();
wui.logParamValue("returnValue", returnValue);

var _name = returnValue.name || "id",
	_maxNumber = parseInt(returnValue.maxNumber),
	_tabUrl = returnValue.tabUrl.split(","),
	_otherParam = {"name" : _name};
var _tagsId = "chosen", _listUrl = "",  // 保存当前显示的列表页面链接
	myTagsinput = null,
	myTabs = null;

/**
 * 初始化页面信息
 * @return {[type]} [description]
 */
var initPageInfo = function(){
	
    if(_tabUrl.length > 0){

        // 转化字符串为对象
        var otherParam = wui.parseToObj(returnValue.otherParam);
        if(otherParam){
            _otherParam = $.extend(true, {}, _otherParam, otherParam);
        }
        _setListHeight();   // 必须先计算高度
        _initTags();
        _initTree();
    }else{
    	wui.warnNotice("没有传入列表地址");
        return;
    }
    
    // 绑定按钮事件
    _bindBtnEvent();
}

/**
 * 初始化tagsinput组件
 * @return {[type]} [description]
 */
var _initTags = function(){
	var params = {
        isFreeInput: false,
        name: _name,
        text: returnValue.text,
        isAllowDuplicates: (parseInt(returnValue.isAllowDuplicates) == 1 ? true: false),
        maxNumber: _maxNumber
    }

    // 转化字符串为对象
    var value = wui.parseToObj(returnValue.value);
    if(value){
        params.value = value;
    }else{
        params.value = [];
    }

    myTagsinput = wui.tagsinput(_tagsId, params);
    myTagsinput.on("itemRemoved", function(e){
        var item = e.item;
        updateSelectedNode(item, false);
    }).on("itemAddError", function(e){ // 监听标签添加失败时，取消子页面节点选中
        wui.warnNotice(e.message);
        var item = e.item;
        updateSelectedNode(item, false);
    }).on("itemRemoveError", function(e){  // 监听标签删除失败时，增加子页面节点选中
        wui.warnNotice(e.message);
        var item = e.item;
        updateSelectedNode(item, true);
    });
}

/**
 * 初始化Tabs组件
 * @param  {JSON} newOtherParam [传递到选项卡页面的参数]
 * @return NULL
 */
var _initTabs = function(newOtherParam){
    var itemsArray = myTagsinput.get("json");
    newOtherParam.data = JSON.stringify(itemsArray);
    var tabTitle = returnValue.tabTitle.split(",");
    var tabPages = [], len = _tabUrl.length;
    for(var i = 0; i < len; i++){
        var tabPage = {};
        tabPage.title = tabTitle[i];
        tabPage.dataType = "iframe";
        tabPage.content = _tabUrl[i];
        tabPage.otherParam = newOtherParam;
        tabPages.push(tabPage);
    }
    if(myTabs){
        myTabs.option({tabPages: tabPages});
    }else{
        myTabs = wui.tabs("tabs", {tabPages: tabPages}).on('beforeShow', function(e){    // 子页面加载之前，将已选数据作为额外传递参数传递到子页面
            var items = myTagsinput.get("json");
            e.otherParam.data = JSON.stringify(items);
            return e;
        }).on("iframeLoaded", function(e){
            // console.info(e.iframeId + "加载完毕");
        });
    }
}

/**
 * 判断Tree并初始化
 * @return NULL
 */
var _initTree = function(){
    if(returnValue.treeUrl != undefined && returnValue.treeUrl != ""){
        $("#treeTitle").text(returnValue.treeTitle);

        var treeParams = {
            loadDataUrl: returnValue.treeUrl,
            // activate: _activate
            activate : function(event, data) {
                var node = data.node;

                // 获取到当前tree选中节点的要传递到列表页面的数据{parentId: node.key}
                _otherParam.parentId = node.key;

                // 重新刷新tabs
                _initTabs(_otherParam);
            }
        }

        var treeDemo = wui.tree("tree" ,treeParams);
    }else{
        $(".ui-selector-tree").hide();
        // 重新刷新tabs
        _initTabs(_otherParam);
    }
}

/**
 * 计算列表高度(必须计算并设置高度，否则Tab里面的iframe高度不会撑满)
 */
var _setListHeight = function(){
    var uiSelectorListHeight = $(".ui-selector-list").height();
    // console.error(uiSelectorListHeight);
    $(".list-full-height").css({
        "height": (uiSelectorListHeight - 60) + "px",
        "width": "100%"
    })
}

/**
 * 绑定按钮事件
 * @return NULL
 */
var _bindBtnEvent = function(){
    $("#btnSubmit").on("click", function(){
        var data = myTagsinput.get("json");
        var params = {
            selectorId: returnValue.selectorId,
            data: data
        }
        wui.closeModalDialog(params);
    })

    $('#btnCancel').click(function(){
        // 取消模态窗口
        wui.cancelModalDialog();
    });
}

/**
 * 更新组件页结果值
 * @param  {json}  node      [更新的节点数据]
 * @param  {Boolean} isChecked [是否选中]
 * @return null
 */
function updateResultNode(node, isChecked){
    isChecked = isChecked || false;
    if(isChecked){
        myTagsinput.add(node);
    }else{
        myTagsinput.remove(node);
    }
}

/**
 * 更新选项卡选中值
 * @param  {json}  node      [更新的节点数据]
 * @param  {Boolean} isChecked [是否选中]
 * @return null
 */
function updateSelectedNode(node, isChecked){
    isChecked = isChecked || false;
    myTabs.index(undefined, function(index, elemnt, iframe){
        if(iframe.contentWindow 
            && iframe.contentWindow.updateSelectedNode 
                && typeof(iframe.contentWindow.updateSelectedNode) === "function"){
            iframe.contentWindow.updateSelectedNode(node, isChecked);
        }
    });
}
