<%
    request.setAttribute("isDev", leap.core.AppContext.current().getConfig().getProperty("runtime.app.isDev"));
	request.setAttribute("contextPath", request.getContextPath());		
	request.setAttribute("staticContext", leap.core.AppContext.current().getConfig().getProperty("runtime.static.contextPath"));
	request.setAttribute("fileContext", leap.core.AppContext.current().getConfig().getProperty("runtime.file.contextPath"));
	request.setAttribute("themes", "default");
%>
<script>

	var Global = {};
	Global.isDev = ${isDev};
	Global.contextPath = '${contextPath}';
	Global.staticContext = '${staticContext}';
	Global.fileContext = '${fileContext}';
	Global.themesContext = Global.contextPath + "/themes/${themes}";
	Global.rootContextPath = document.location.protocol + "//" + document.location.host;
	
</script>