/**
 * 日期：2018-04-14
 * 作者：feily
 * 描述：首页业务服务类
 */
var indexService = (function () {
	
	var _isDev = true;
	var _className = "indexService";
	
	var _url = "";
	
    /**
     * 获取当前登陆用户信息
	 * @param success function: 操作成功回调函数
	 * @param fail function: 操作失败回调函数
     */
    var _getCurrentUserInfo = function (success, fail) {
        wui.logMethodCalled("_getCurrentUserInfo" ,_className);
        
        var url = "";
        if(_isDev){
        	url = Global.contextPath + "/scripts/index/data/_getCurrentUserInfo_$user$.json";
        	
        	var user = "admin";
        	var queryParam = wui.getQueryString();
        	if(queryParam.user){
        		user = queryParam.user;
        	}
        	url = url.replace("$user$", user);
        }
       
        var param = {
        	url: url,
        	successCallback: success,
        	errorCallback: fail
        }
        wui.ajax(param);
    };
    
    // 对外调用方法
    return {

        // 获取当前登陆用户信息
    	getCurrentUserInfo: _getCurrentUserInfo,
    }
    
})();
