/**
 * 日期：2018-04-14
 * 作者：feily
 * 描述：首页页面控制类
 */

var _menus = []; // 全局菜单列表
var _className = "indexController";
    	
/**
 * 加载页面数据信息
 * @param res json: 页面信息对象
 *          res.user json: 用户信息
 *          res.menu json: 菜单信息
 */
 function loadPageInfo(res){
	 wui.logMethodCalled("loadPageInfo" ,_className);
	 wui.logParamValue("res", res, _className);
	 
	 // 加载菜单信息
	 var menu = res.menu;
	 _loadMenuInfo(menu);
	 
	 // 加载用户信息
	 var user = res.user;
	 _loadUserInfo(user);
}

/**
 * 加载用户信息
 * @param user json: 用户信息对象
 *          user.userName string: 用户姓名
 *          user.orgName string: 组织名称
 *          user.userPictuerUrl string: 用户头像
 */
function _loadUserInfo(user){
	wui.logMethodCalled("_loadUserInfo" ,_className);
	
	// 设置用户姓名
	$("#spanUserName").text(user.userName);
	
	// 设置头像
	var userPictuerUrl = user.userPictuerUrl || Global.themesContext + "/images/avatar.png";
	$("#imguserPictuerUrl").attr("src", userPictuerUrl);
}

/**
 * 加载菜单信息
 * @param menu json: 菜单信息
 *          menu.userName string: 用户姓名
 *          menu.orgName string: 组织名称
 *          menu.userPictuerUrl string: 用户头像
 *          menu.childs json[]: 子菜单列表
 */
function _loadMenuInfo(menu){
	wui.logMethodCalled("_loadMenuInfo" ,_className);
	
	// 设置全局菜单变量
	if(menu.childs){
		_menus = menu.childs;
		
    	// 根据菜单数据和模板渲染导航
        $("#ulNavMenu").html(template('navMenuTemplate', menu));
		
		// 给导航菜单添加点击事件
        $("#ulNavMenu [_menuName]").click(function(event) {
        	var $this = $(this);
        	console.info($this);
        	// 切换激活样式
        	$this.parents('li.root-nav').eq(0)
	            .addClass("root-nav-active")
	            .siblings()
	            .removeClass("root-nav-active");
        	// 删除激活样式
//        	$("#ulNavMenu li").removeClass("root-nav-active");
             		
        	
//        	$this.addClass("root-nav-active");
            var menuName = $this.attr('_menuName');
            if(!!menuName){
            	for (var i = 0; i < _menus.length; i++) {
					if(menuName == _menus[i].name){
						var theMenu = _menus[i];
						if(theMenu.childs && theMenu.childs.length > 0){
							// 加载边栏菜单
							_loadSidebarMenuInfo(_menus[i]);
						}else if(theMenu.id == "btnFullScreen"){
							// 全屏按钮点击切换事件
							if($this.data("isfullscreen")){
				                $("body").removeClass("page-full-width");
				                $(".page-header-inner > .sidebar-toggler").show();
				                $this.data("isfullscreen", false);
				                $this.attr("title", "全屏页面演示").text().replace("全屏页面演示", "取消全屏页面演示");
				            }else{
				                $("body").addClass("page-full-width");
				                $(".page-header-inner > .sidebar-toggler").hide();
				                $this.data("isfullscreen", true);
				                $this.attr("title", "取消全屏页面演示").text().replace("取消全屏页面演示", "全屏页面演示");
				            }
						}
						break;
					}
				}
            }
        });
        
        // 加载第一个菜单URL
        $("#ulNavMenu [_menuName]").eq(0).click();
	}else{
		alert("menu数据结构错误，请核实！");
	}
}// end function _loadMenuInfo(menu)
 
/**
 * 加载边栏菜单信息
 * @param menu json: 菜单信息
 *          menu.userName string: 用户姓名
 *          menu.orgName string: 组织名称
 *          menu.userPictuerUrl string: 用户头像
 *          menu.childs json[]: 子菜单列表
 */
function _loadSidebarMenuInfo(menu){
	wui.logMethodCalled("_loadSidebarMenuInfo" ,_className);
	
	/**
     * 菜单伸缩方法
	 * @param $this: a标签对象
     */
    var _toggleMenu = function($this){
    	
        var hasChild = $this.attr('_hasChild') == "true";
        // console.info(hasChild);
        if(hasChild){
            
            // 判断是否被打开
            if($this.parent().hasClass('open')){
                // 清除选中标记
                $this.parent().removeClass('open');
                $this.find('.arrow').removeClass('open');
                
                // 隐藏子菜单
                var subMenu = $this.siblings();
                subMenu.slideToggle();
            }else{
                // 新增选中标记
                $this.parent().addClass('open');
                $this.find('.arrow').addClass('open');

                // 显示子菜单
                var subMenu = $this.siblings();
                subMenu.slideToggle();
            }
            
        }else{
            // 清除菜单选中标记
        	$("#page-sidebar-menu a").each(function(i){
                if($(this).attr('_hasChild') != "true"){
                    $(this).parent().removeClass('open');
                }
            });
            $this.parent().addClass('open');
            
            // 根据URL加载Iframe内容页面
            var url = $this.attr('_href');
            _loadIframe(url);
        }
    }; // end function _toggleMenu
    
	if(menu.childs && menu.childs.length > 0){

//		$(".page-sidebar-wrapper").show();
		
        // 根据菜单数据和模板渲染菜单
        $("#page-sidebar-menu").html(template('sidebarMenuTemplate', menu));
        $("body").removeClass("page-full-width");
     	// 给菜单添加点击事件
        $("#page-sidebar-menu a").click(function(event) {
            var $this = $(this);
            var url = $this.attr('_href');
            if(!!url){
                _loadIframe(url);
            }
        });
        
        // 加载第一个菜单URL
        var isFirst = false;
        $("#page-sidebar-menu a").each(function(i){
            if(!isFirst){
                var $this = $(this);
                if($this.attr('_hasChild') != "true"){
                    isFirst = true; 
                }
                _toggleMenu($this);
            }
        });
	}else{
		// 没有子菜单，全屏显示url
//		$(".page-sidebar-wrapper").hide();
//		$(".page-content-wrapper").width("100%");		
//		$("#page-sidebar-menu").html("");
		$("body").addClass("page-full-width");
        $(".page-header-inner > .sidebar-toggler").hide();
		
		var url = menu.url;
        if(!!url){
            _loadIframe(url);
        }else{
        	alert("menu.url不能为空！");
        }
	}
    
} // end function _loadSidebarMenuInfo(menu)

/**
 * 根据URL加载Iframe内容页面
 * @param url string: 显示页面url
 */
function _loadIframe(url){
    var iframe = $('#content-iframe');
    if(iframe.attr('src') != url){
        iframe.attr('src', url);
    }
} // end function _loadIframe(url)
