/**
* 日期：2018-04-10
* 作者：Chenzx
* 版本：0.0.1
* 描述：Debug配置文件。此脚本，将作为唯一修改的配置文件
*/
var debugConfig = {
	isDev : true,	// 是否开启调试模式
	commonPageBasePath: "/bingosoft-framework-demo-web/base/common/",		// 公共页面存放文件夹路径
//	hostAddress: "http://111.230.47.180:7070", // 后台服务地址
	// hostAddress: "http://localhost:7070",	// 后台服务地址
	// hostAddress : "http://localhost:53890", // 后台服务地址
	isLog : true,    // 是否打印参数,此参数可以一次性关闭下列四类日志的打印
	isLogMethodCalled : false,    // 是否打印方法日志
	isLogParamValue : false,      // 是否打印参数日志
	isLogMessage : true,         // 是否打印消息日志
	isLogError : true      		  // 是否打印错误日志
}

/* debugConfig 合并到 wui 中 */
$.extend(true, wui, debugConfig);