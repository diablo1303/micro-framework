package bingosoft.metro.base.form;

import leap.web.form.FormBase;

/**
 * @author Chenzx
 * @功能描述 上传图片模块类
 * @创建时间 2016-12-28
 */
public class ImageForm extends FormBase {

	private String resolution;
	private String imageData;

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getImageData() {
		return imageData;
	}

	public void setImageData(String imageData) {
		this.imageData = imageData;
	}
}
