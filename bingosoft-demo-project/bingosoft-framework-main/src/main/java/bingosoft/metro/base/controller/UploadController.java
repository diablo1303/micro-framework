package bingosoft.metro.base.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import bingosoft.components.base.controller.BaseController;
import bingosoft.components.base.service.ServiceResult;
import bingosoft.metro.base.form.FileForm;
import bingosoft.metro.base.service.UploadService;
import leap.lang.json.JSON;
import leap.web.annotation.Path;

/**
 * @author Feily
 * @功能描述 上传文件控制类
 * @创建时间 2016-12-08
 */
@Path("/base/upload")
public class UploadController extends BaseController {

	/** 上传文件服务类 */
	private UploadService uploadService = new UploadService();

	/**
	 * 构建函数
	 */
	public UploadController() {
		this.setLogClass(getClass());
		this.logMethodCalled("UploadController-构建函数");
	}

	/**
	 * 执行上传操作
	 * 
	 * @param subPath 上传子路径
	 * @param isRename 上传文件是否重命名
	 * @return
	 */
	@Path("exec")
	public ServiceResult exec(String subPath, Boolean isRename, String isBase64, String images) {
		this.logMethodCalled("exec");
		this.logParamValue("subPath", subPath);
		this.logParamValue("isRename", isRename);
		this.logParamValue("isBase64", isBase64);
		
		if(isBase64 == "1"){
			return this.crop(images, subPath, isRename);
		}else{
			this.uploadService.setSubPath(subPath);
			if (isRename != null) {
				this.uploadService.setRename(isRename);
			}
	
			ServiceResult serviceResult = new ServiceResult();
	
			try {
				// 上传文件
				HttpServletRequest request = request().getServletRequest();
				List<FileForm> list = uploadService.uploadFiles(request);
				if (list.isEmpty()) {
					serviceResult.setIsSuccess(false);
					serviceResult.setMessage("上传失败！");
				} else {
					serviceResult.setMessage("上传成功！");
					serviceResult.setBusinessObject(list);
				}
	
			} catch (Exception ex) {
				// return this.buildExceptionResult(ex);
				serviceResult.setIsSuccess(false);
				serviceResult.setMessage(ex.getMessage());
			}
	
			return serviceResult;
		}
	}

	/**
	 * 执行上传操作，返回text数据(主要是因为IE8/IE9下返回json数据会提示下载json文件)
	 * 
	 * @param subPath
	 *            上传子路径
	 * @param isRename
	 *            上传文件是否重命名
	 * @return
	 */
	@Path("execute")
	public String execute(String subPath, Boolean isRename) {
		this.logMethodCalled("execute");
		this.logParamValue("subPath", subPath);
		this.logParamValue("isRename", isRename);

		this.uploadService.setSubPath(subPath);
		if (isRename != null) {
			this.uploadService.setRename(isRename);
		}

		ServiceResult serviceResult = new ServiceResult();

		try {
			// 上传文件
			HttpServletRequest request = request().getServletRequest();
			List<FileForm> list = uploadService.uploadFiles(request);
			if (list.isEmpty()) {
				serviceResult.setIsSuccess(false);
				serviceResult.setMessage("上传失败！");
			} else {
				serviceResult.setMessage("上传成功！");
				serviceResult.setBusinessObject(list);
			}

		} catch (Exception ex) {
			return ex.getMessage();
		}
		return JSON.stringify(serviceResult);
	}

	/**
	 * 执行图片上传操作
	 * 
	 * @param image
	 *            截取的图片编码
	 * @param subPath
	 *            上传子路径
	 * @param isRename
	 *            上传文件是否重命名
	 * @return
	 */
	@Path("crop")
	public ServiceResult crop(String images, String subPath, Boolean isRename) {
		this.logMethodCalled("crop");
		this.logParamValue("subPath", subPath);
		this.logParamValue("isRename", isRename);

		this.uploadService.setSubPath(subPath);
		if (isRename != null) {
			this.uploadService.setRename(isRename);
		}

		ServiceResult serviceResult = new ServiceResult();

		try {
			serviceResult = this.uploadService.buildImage(images);

		} catch (Exception ex) {
			serviceResult.setIsSuccess(false);
			serviceResult.setMessage(ex.getMessage());
		}
		return serviceResult;
	}
}
