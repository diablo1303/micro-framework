package bingosoft.metro.base.form;

import leap.web.form.FormBase;

/**
 * @author Feily
 * @功能描述 上传文件模块类
 * @创建时间 2016-12-05
 */
public class FileForm extends FormBase {

	private String name;
	private String filePath;
	private String contentType;
	private Long size;
	private String fileUrl;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public String getFileUrl() {
		return fileUrl;
	}
	public void setFileUrl(String filePath) {
		this.fileUrl = "http://10.200.203.181:9393/" + filePath;
	}
}
