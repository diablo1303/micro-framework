package bingosoft.metro;

import bingosoft.components.base.helper.AppHelper;
import leap.core.annotation.Inject;
import leap.lang.logging.LogFactory;
import leap.lang.naming.NamingStyles;
import leap.web.App;
import leap.web.json.JsonConfigurator;

/**
 * @author Feily
 * @功能描述 应用全局对象
 * @创建时间 2016-11-08
 */
public class Global extends App {

	@Inject
    protected JsonConfigurator jc;

	/**
	 * 构建函数
	 */
	public Global() {
		this.log = LogFactory.get(this.getClass());
	}

	/**
	 * 应用初始化方法
	 */
	@Override
	protected void init() throws Throwable {
		super.init();
		
		// 初始化App全局参数
		this.initAppHelper();
		
		// 设置JSON序列化的格式
		this.setJsonStyle();
	}

	/**
	 * 设置JSON序列化的格式
	 */
	private void setJsonStyle() {
        jc.setDefaultNamingStyle(NamingStyles.LOWER_CAMEL);
        jc.setDefaultDateFormat("yyyy-MM-dd HH:mm:ss");
        //jc.setDefaultSerializationIgnoreNull(true);
	}
	
	/**
	 * 初始化App全局参数
	 */
	protected void initAppHelper() {
		this.log.debug("initAppHelper Called");
		
//		this.log.error("【参数提示】" + JSON.stringify(this.config));
		// 初始化 AppHelper
		AppHelper.setConfig(this.config);
		AppHelper.setContext(this.context);
		AppHelper.setFactory(this.factory);
		AppHelper.setHome(this.home);
	}
}