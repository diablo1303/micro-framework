package bingosoft.metro.code.service;

import java.util.List;

import org.junit.Test;

import bingosoft.components.base.helper.LogHelper;
import bingosoft.metro.code.model.DbColumnModel;
import bingosoft.metro.code.model.DbTableModel;
import leap.core.junit.AppTestBase;
import leap.lang.Assert;

public class DatabaseServiceTest extends AppTestBase {

	private IDatabaseService databaseService = new DatabaseService();
	
	@Test
	public void testGetTable() {
		
		String tableName = "demo_user";
		DbTableModel table = this.databaseService.getTable(tableName);
		
		Assert.isTrue(table != null);
		LogHelper.println("table", table);
	}

	@Test
	public void testQueryAllTables() {
		List<DbTableModel> tables = this.databaseService.queryAllTables();
		
		Assert.isTrue(tables != null);
		LogHelper.println("tables", tables);
	}

	@Test
	public void testQueryColumns() {
		String tableName = "demo_user";
		List<DbColumnModel> columns = this.databaseService.queryColumns(tableName);
		
		Assert.isTrue(columns != null);
		LogHelper.println("columns", columns);
	}
}
