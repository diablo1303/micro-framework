package bingosoft.metro.code.model;

import bingosoft.metro.code.helper.CodeHelper;
import leap.orm.model.Model;

/**
 * @author Feily
 * @功能描述 数据库表模块类
 * @创建时间 2017-09-22
 */
public class DbTableModel extends Model{
	
	private String tableName;
	private String tableType;
	private String tableComment;
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getTableType() {
		return tableType;
	}
	public void setTableType(String tableType) {
		this.tableType = tableType;
	}
	public String getTableComment() {
		return tableComment;
	}
	public void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}

	public String getHumpTableName() {
		return CodeHelper.getHumpName(tableName);
	}
	public void setHumpTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public String getCapitalTableName() {
		return CodeHelper.getCapitalHumpName(tableName);
	}
	public void setCapitalTableName(String tableName){
		this.tableName = tableName;
	}
}
