package bingosoft.metro.code.helper;

/**
 * @author Feily
 * @功能描述 代码帮助类
 * @创建时间 2017-09-22
 */
public class CodeHelper {

	/**
	 * 获取大写驼峰格式名称
	 * 
	 * @param name
	 * @return
	 */
	public static String getCapitalHumpName(String name) {

		String formatName = "";

		String[] names = name.toLowerCase().split("_");
		for (int i = 0; i < names.length; i++) {
			String item = names[i];
			item = item.substring(0, 1).toUpperCase() + item.substring(1);
			formatName += item;
		}

		return formatName;
	}

	/**
	 * 获取驼峰格式名称
	 * 
	 * @param name
	 * @return
	 */
	public static String getHumpName(String name) {

		String formatName = "";

		String[] names = name.toLowerCase().split("_");
		for (int i = 0; i < names.length; i++) {
			String item = names[i];
			if (i > 0) {
				item = item.substring(0, 1).toUpperCase() + item.substring(1);
			}
			formatName += item;
		}

		return formatName;
	}

	/**
	 * 获取java数据类型
	 * 
	 * @param dataType
	 * @param dataPrecision
	 * @param dataScale
	 * @return
	 */
	public static String getDataType(String dataType, Integer dataPrecision, Integer dataScale) {

		String lDataType = dataType.toLowerCase();
		if (lDataType.contains("char") || lDataType.contains("varchar") || lDataType.equals("nvarchar")) {
			return "String";
		} else if (lDataType.equals("date") || lDataType.equals("timestamp") || lDataType.equals("datetime") || lDataType.equals("datetime2")) {
			return "Date";
		} else if (lDataType.equals("int4") || lDataType.equals("int") || lDataType.equals("tinyint")
				|| lDataType.equals("bigint")) { // postgreSql sqlServer
			return "Integer";
		} else if (lDataType.equals("bool")) { // postgreSql
			return "Boolean";
		} else if (lDataType.equals("number") || lDataType.equals("numeric")) {
			if (dataScale == 0) {
				if (dataPrecision == 1) {
					return "Boolean";
				} else {
					return "Integer";
				}
			} else {
				return "Double";
			}
		} else if (lDataType.equals("decimal")) { //sqlServer
			return "Integer";
		}
		return "String";
	}
}
