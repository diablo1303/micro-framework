package bingosoft.metro.code.service;

import java.util.List;

import bingosoft.metro.code.model.DbColumnModel;
import bingosoft.metro.code.model.DbTableModel;

/**
 * @author Feily
 * @功能描述 数据库服务接口
 * @创建时间 2017-09-22
 */
public interface IDatabaseService {

	/**
	 * 获取所有表对象
	 * @return
	 */
	DbTableModel getTable(String tableName);

	/**
	 * 获取所有表对象
	 * @return
	 */
	List<DbTableModel> queryAllTables();

	/**
	 * 根据表名获取列对象
	 * @param tableName
	 * @return
	 */
	List<DbColumnModel> queryColumns(String tableName);
}