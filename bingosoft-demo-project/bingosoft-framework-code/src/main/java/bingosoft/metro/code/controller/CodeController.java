package bingosoft.metro.code.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import bingosoft.components.base.controller.BaseController;
import bingosoft.metro.code.service.IDatabaseService;
import leap.core.annotation.Inject;
import leap.core.annotation.M;
import leap.web.annotation.Path;
import leap.web.view.ViewData;

/**
 * @author Feily
 * @功能描述 代码控制类
 * @创建时间 2017-09-22
 */
@Path("/code")
public class CodeController extends BaseController {

	@Inject @M
	private IDatabaseService databaseService;
	
	private String author;
	private String date;
	
	/**
	 * 构建函数
	 */
	public CodeController(){
		this.setLogClass(this.getClass());
		
		this.author = "feily";
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		date = sdf.format(new Date());
	}
	
    public void index(ViewData vd, String tableName){
    	this.logMethodCalled("index");
    	
    	vd.put("tables", this.databaseService.queryAllTables());
    }
    
    /**
     * Model模板页
     * @param vd
     * @param tableName
     */
    public void model(ViewData vd, String tableName){
    	this.logMethodCalled("model");
    	
    	this.initParameters(vd, tableName);
    }
    
    /**
     * Form模板页
     * @param vd
     * @param tableName
     */
    public void form(ViewData vd, String tableName){
    	this.logMethodCalled("form");
    	
    	this.initParameters(vd, tableName);
    }
    
    /**
     * IService模板页
     * @param vd
     * @param tableName
     */
    public void iservice(ViewData vd, String tableName){
    	this.logMethodCalled("iservice");
    	
    	this.initParameters(vd, tableName);
    }
    
    /**
     * Service模板页
     * @param vd
     * @param tableName
     */
    public void service(ViewData vd, String tableName){
    	this.logMethodCalled("service");
    	
    	this.initParameters(vd, tableName);
    }
    
    /**
     * TService模板页
     * @param vd
     * @param tableName
     */
    public void tservice(ViewData vd, String tableName){
    	this.logMethodCalled("tservice");
    	
    	this.initParameters(vd, tableName);
    }
    
    /**
     * Api模板页
     * @param vd
     * @param tableName
     */
    public void api(ViewData vd, String tableName){
    	this.logMethodCalled("api");
    	
    	this.initParameters(vd, tableName);
    }
    
    /**
     * TApi模板页
     * @param vd
     * @param tableName
     */
    public void tapi(ViewData vd, String tableName){
    	this.logMethodCalled("tapi");
    	
    	this.initParameters(vd, tableName);
    }
    
    /**
     * 初始化参数
     * @param vd
     * @param tableName
     */
	private void initParameters(ViewData vd, String tableName) {

		vd.put("author", this.author);
		vd.put("date", this.date);
    	vd.put("table", this.databaseService.getTable(tableName));
        vd.put("columns",this.databaseService.queryColumns(tableName));
        vd.put("pkCol", this.databaseService.queryColumns(tableName).get(0));
	}
}

