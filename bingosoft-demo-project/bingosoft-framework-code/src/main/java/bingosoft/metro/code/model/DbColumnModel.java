package bingosoft.metro.code.model;

import bingosoft.metro.code.helper.CodeHelper;
import leap.orm.model.Model;

/**
 * @author Feily
 * @功能描述 数据库列模块类
 * @创建时间 2017-09-22
 */
public class DbColumnModel extends Model{
	
	private String tableName;
	private Integer columnId;
	private String columnName;
	private String dataType;
	private Integer dataLength;
	private Integer dataPrecision;
	private Integer dataScale;
	private Boolean nullable;
	private String columnComment;
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public Integer getColumnId() {
		return columnId;
	}
	public void setColumnId(Integer columnId) {
		this.columnId = columnId;
	}
	public String getColumnName() {
		return CodeHelper.getHumpName(columnName);
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getDataType() {
		return CodeHelper.getDataType(this.dataType, this.dataPrecision, this.dataScale);
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public Integer getDataLength() {
		return dataLength;
	}
	public void setDataLength(Integer dataLength) {
		this.dataLength = dataLength;
	}
	public Integer getDataPrecision() {
		return dataPrecision;
	}
	public void setDataPrecision(Integer dataPrecision) {
		this.dataPrecision = dataPrecision;
	}
	public Integer getDataScale() {
		return dataScale;
	}
	public void setDataScale(Integer dataScale) {
		this.dataScale = dataScale;
	}
	public Boolean getNullable() {
		return nullable;
	}
	public void setNullable(Boolean nullable) {
		this.nullable = nullable;
	}
	public String getColumnComment() {
		return columnComment;
	}
	public void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}
	
	public String getCapitalColumnName() {
		return CodeHelper.getCapitalHumpName(columnName);
	}
	public void setCapitalColumnName(String columnName) {
		this.columnName = columnName;
	}
}
